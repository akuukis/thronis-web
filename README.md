Tronis.lv
================================================================================

Git commands. Use in this order:
- `git fetch` to pull commits without merge
- `git rebase origin/master` join 2 commits with your commit on top of existing
- `git push`

About commit messages ([learn more](https://www.conventionalcommits.org/en/v1.0.0/)):
`type(scope): title`


Build locally
```
yarn start
```


Deploy
```
yarn deploy
```

#test