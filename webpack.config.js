/* eslint-env node */
/* eslint-disable import/no-unused-modules */
/* eslint-disable @typescript-eslint/no-var-requires */
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const { join, resolve } = require('path')
const webpack = require('webpack')
require('dotenv').config()


const ROOT_DIR = path.resolve(__dirname)
const SRC_DIR = path.join(ROOT_DIR, './src')
const STATIC_DIR = path.join(ROOT_DIR, './static')
const IS_PRODUCTION = process.argv.some((arg) => arg === 'production' || arg === '--production')

console.log(resolve(__dirname, 'static', 'files'))

module.exports = {
    mode: IS_PRODUCTION ? 'production' : 'development',
    entry: {
        main: [
            './src/index',
        ],
    },
    output: {
        publicPath: '/',
    },
    devtool: IS_PRODUCTION ? false : 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        alias: {
            'assets': path.join(ROOT_DIR, 'assets'),
            'src/common': path.join(SRC_DIR, 'common'),
            'src/components': path.join(SRC_DIR, 'components'),
            'src/constants': path.join(SRC_DIR, 'constants'),
            'src/layouts': path.join(SRC_DIR, 'layouts'),
            'src/stores': path.join(SRC_DIR, 'stores'),
        },
        fallback: { 'path': require.resolve('path-browserify') },
    },
    // node: {
    //     __filename: true,  // To simplify makeStyles naming.
    // },
    module: {
        rules: [
            { test: /\.css$/, use: [ { loader: 'style-loader' }, { loader: 'css-loader' } ] },
            { test: /\.html$/, use: { loader: 'html-loader' } },
            { test: /\.svg$/, use: { loader: 'file-loader', options: { name: 'img/[name].[ext]?[contenthash]' } } },
            { test: /\.pdf$/, use: { loader: 'file-loader', options: { name: 'files/[name].[ext]?[contenthash]' } } },
            { test: /\.jpg$/, use: { loader: 'file-loader', options: { name: 'img/[name].[ext]?[contenthash]' } } },
            { test: /\.png$/, use: { loader: 'file-loader', options: { name: 'img/[name].[ext]?[contenthash]' } } },
            { test: /\.ttf$/, use: { loader: 'file-loader', options: { name: 'fonts/[name].[ext]?[contenthash]' } } },

            {
                test: /\.(j|t)sx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true,
                        babelrc: false,
                        presets: [
                            [
                                '@babel/preset-env',
                                { targets: { browsers: '> 1%' } }, // or whatever your project requires
                            ],
                            ['@babel/preset-typescript', { allowNamespaces: true }],
                            '@babel/preset-react',
                        ],
                        plugins: [
                            // plugin-proposal-decorators is only needed if you're using experimental decorators in TypeScript
                            ['@babel/plugin-proposal-decorators', { legacy: true }],
                            ['@babel/plugin-proposal-class-properties', { loose: true }],
                            ['@babel/plugin-proposal-optional-chaining'],
                            ['@babel/plugin-proposal-nullish-coalescing-operator'],
                            !IS_PRODUCTION && require.resolve('react-refresh/babel'),
                        ].filter(Boolean),
                    },
                },
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(IS_PRODUCTION ? 'production' : 'development'),
        }),
        new ForkTsCheckerWebpackPlugin({
            logger: {
                issues: process.argv.includes('--json') ? 'silent' : 'console',
            },
            typescript: { configFile: 'tsconfig.json' },
        }),
        new HtmlWebpackPlugin({
            title: 'LARTS Throne',
            template: path.join(STATIC_DIR, 'index.ejs'),
        }),
        new FaviconsWebpackPlugin(path.join(STATIC_DIR, 'favicon.svg')),
        !IS_PRODUCTION && new ReactRefreshWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                { from: resolve(__dirname, 'static', 'files'), to: '[name][ext]?[contenthash]' },
            ],
        }),
    ].filter(Boolean),

    devServer: {
        https: true,
        historyApiFallback: true,  // for SPA to fallback to index.
    },

}
