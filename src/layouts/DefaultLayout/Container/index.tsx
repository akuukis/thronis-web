import React, { ComponentProps } from 'react'

import { Divider, Grid, Paper, PaperProps } from '@material-ui/core'

import { IMyTheme, createFC, createStyles, useBreakpoint } from 'src/common'

import Footer from './Footer'
import Outline from './Outline'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        minWidth: 320,
        backgroundColor: theme.palette.primary.background,
        position: 'relative',
    },
    main: {
        flexGrow: 1,
        background: theme.palette.background.default,
        padding: theme.spacing(0),
        transition: theme.transitions.create('padding'),
        [theme.breakpoints.up('sm')]: {
            padding: theme.spacing(8),
        },
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(12),
        },
    },
})


interface IProps extends ComponentProps<typeof Outline> {
    mainProps?: PaperProps
    banner?: JSX.Element
    footer?: JSX.Element
}


export default createFC(import.meta.url, styles)<IProps>(({ children, classes, theme, ...props }) => {
    const { mainProps, banner, footer, ...outlineProps } = props
    const mdUp = useBreakpoint('md')

    return (
        <Grid
            container
            className={classes.root}
            direction={mdUp ? 'row' : 'column'}
            wrap='nowrap'
            justifyContent="center"
            justifyItems="stretch"
        >
            <Divider flexItem orientation="vertical" sx={{ display: { xs: 'none', xl: 'block' } }} />
            <Outline {...outlineProps} />
            <Divider flexItem orientation={mdUp ? 'vertical' : 'horizontal'} />
            <Grid item xs container direction='column' wrap='nowrap' sx={{ position: 'relative', maxWidth: theme.breakpoints.values.xl - 300 - 1 }}>
                {banner && mdUp && banner}
                <Paper component='main' className={classes.main} {...mainProps}>
                    {children}
                </Paper>
                {footer}
                <Footer />
            </Grid>
            <Divider flexItem orientation="vertical" sx={{ display: { xs: 'none', xl: 'block' } }} />
        </Grid>
    )
}) /* ============================================================================================================== */
