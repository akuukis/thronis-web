import React from 'react'

import { Box, Grid, Typography } from '@material-ui/core'

import { createFC, useBreakpoint } from 'src/common'
import IconDiscord from 'src/components/icons/IconDiscord'
import IconEmail from 'src/components/icons/IconEmail'
import IconFacebook from 'src/components/icons/IconFacebook'
import IconInstagram from 'src/components/icons/IconInstagram'
import IconPhone from 'src/components/icons/IconPhone'
import { UniversalLink } from 'src/components/UniversalLink'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const mdUp = useBreakpoint('md')

    return (
        <Box sx={{
            position: 'relative',
            backgroundColor: theme.palette.primary.dark,
            color: theme.palette.primary.contrastText,
            minHeight: 122,
            padding: theme.spacing(2, 4),
            [theme.breakpoints.up('sm')]: {
                backgroundColor: theme.palette.primary.main,
                height: 122,
            },
        }}
        >
            <Grid container sx={{ height: 'calc(100% + 32px)', [theme.breakpoints.down('sm')]: { alignItems: 'flex-end' } }} spacing={4} wrap='nowrap' alignItems='center'>
                <Grid
                    item
                    container
                    sx={{ width: 'unset', [theme.breakpoints.down('md')]: { flexDirection: 'column' } }}
                    spacing={mdUp ? 2 : 0}
                    wrap='nowrap'
                    alignItems={mdUp ? 'center' : undefined}
                >
                    <Grid item>
                        <Typography color='inherit' sx={{ [theme.breakpoints.down('md')]: { display: 'none' } }}>Tronis XI galvenais organizators:</Typography>
                        <Typography color='inherit'>Kristaps Krūtainis</Typography>
                    </Grid>
                    <Grid item>
                        <Typography color='inherit' noWrap><IconPhone sx={{ verticalAlign: 'middle' }} />{' '}+371 26471183</Typography>
                        <Typography color='inherit' noWrap><IconEmail sx={{ verticalAlign: 'middle' }} />{' '}kkrutainis@gmail.lv</Typography>
                    </Grid>
                </Grid>
                <Box sx={{ flexGrow: 1 }} />
                <Grid item>
                    <UniversalLink href="https://www.facebook.com/LARTS.Throne" color='inherit'>
                        <IconFacebook fontSize='large' sx={{ display: 'block', margin: 'auto', [theme.breakpoints.down('sm')]: { fontSize: 24 } }} />
                        <Typography color='inherit' variant='caption' align='center' noWrap sx={{ [theme.breakpoints.down('sm')]: { display: 'none' } }}>
                            /LARTS.Throne
                        </Typography>
                    </UniversalLink>
                </Grid>
                <Grid item>
                    <UniversalLink href="https://discord.gg/D8ZSqjee4B" color='inherit'>
                        <IconDiscord fontSize='large' sx={{ display: 'block', margin: 'auto', [theme.breakpoints.down('sm')]: { fontSize: 24 } }} />
                        <Typography color='inherit' variant='caption' align='center' noWrap sx={{ [theme.breakpoints.down('sm')]: { display: 'none' } }}>
                            LARTS Throne
                        </Typography>
                    </UniversalLink>
                </Grid>
                <Grid item>
                    <UniversalLink href="https://www.instagram.com/tronis_larp" color='inherit'>
                        <IconInstagram fontSize='large' sx={{ display: 'block', margin: 'auto', [theme.breakpoints.down('sm')]: { fontSize: 24 } }} />
                        <Typography color='inherit' variant='caption' align='center' noWrap sx={{ [theme.breakpoints.down('sm')]: { display: 'none' } }}>
                            /tronis_larp
                        </Typography>
                    </UniversalLink>
                </Grid>

            </Grid>
        </Box>
    )
}) /* ============================================================================================================== */
