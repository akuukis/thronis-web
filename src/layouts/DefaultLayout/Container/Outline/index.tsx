/* eslint-disable react/forbid-dom-props */
import React from 'react'

import { Box, Grid, List, ListItem, ListItemText, Typography } from '@material-ui/core'

import backgroundMd from 'assets/layout/backgroundMd.png'
import backgroundXs from 'assets/layout/backgroundXs.png'

import { createFC } from 'src/common'
import useGoogleTranslateBarVisibility from 'src/common/useGoogleTranslateBarVisibility'


interface TableOfContentEntry {
    label: React.ReactNode,
    hash: string,
}

interface IProps {
    overline?: React.ReactNode,
    title?: string,
    description?: React.ReactNode,
    toc?: TableOfContentEntry[],
}


export default createFC(import.meta.url)<IProps>(({ children, theme, ...props }) => {
    const { overline, title, description, toc } = props
    const googleTranslateBarVisibility = useGoogleTranslateBarVisibility()

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: theme.palette.primary.background,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundImage: `url(${backgroundXs})`,
                backgroundPosition: 'bottom -37px right 0px',
                transition: theme.transitions.create('padding'),
                flexShrink: 0,
                [theme.breakpoints.up('md')]: {
                    width: 300,
                    backgroundImage: `url(${backgroundMd})`,
                    backgroundPosition: 'center right',
                    backgroundSize: 'auto',
                    top: googleTranslateBarVisibility ? 72 + 38 : 72,  // Toolbar height.
                    position: 'sticky',
                    height: `calc(100vh - ${googleTranslateBarVisibility ? 72 + 38 : 72}px)`,  // Toolbar height.
                },
            }}
        >
            <Box sx={{
                flexGrow: 1,
                padding: theme.spacing(2, 1),
                [theme.breakpoints.up('sm')]: {
                    padding: theme.spacing(8, 3),
                },
            }}
            >
                {overline && <Typography variant='overline'>{overline}</Typography>}
                {title && <Typography variant='h1'>{title}</Typography>}
                {description && <Typography paragraph>{description}</Typography>}
                {toc && (
                    <List>
                        {toc.map((tocEntry) => (
                            <ListItem button href={tocEntry.hash}>
                                <ListItemText>{tocEntry.label}</ListItemText>
                            </ListItem>
                        ))}
                    </List>
                )}
            </Box>
            <Grid
                container
                sx={{
                    height: 122,
                    backgroundColor: theme.palette.primary.dark,
                    display: 'none',
                    [theme.breakpoints.up('md')]: {
                        display: 'flex',
                    },
                }}
                alignItems="center"
                justifyContent="center"
            >
                <Typography sx={{ color: theme.palette.primary.contrastText }}>
                    2025
                </Typography>
            </Grid>
        </Box>
    )
}) /* =============================================================================================================== */
