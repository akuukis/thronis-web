import React from 'react'

import { Box, BoxProps, Divider, Typography } from '@material-ui/core'
import { SxProps } from '@material-ui/system'

import { IMyTheme, createFC, createStyles } from 'src/common'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        padding: theme.spacing(2, 2, 4, 2),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(2, 0, 4, 0),
        },
        '&:first-of-type': {
            paddingTop: theme.spacing(2),
            [theme.breakpoints.up('md')]: {
                padding: theme.spacing(0),
            },
        },
    },
})


interface IProps extends BoxProps {
    id?: string
    heading?: string
    whiteHeading?: boolean
    overline?: string
    sx?: SxProps
}


export default createFC(import.meta.url, styles)<IProps>(({ children, classes, theme, ...props }) => {
    const { id: idRaw, heading, overline, whiteHeading, ...sectionProps } = props

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const id = (overline ?? heading ?? idRaw!).replaceAll(' ', '-')

    return (
        <Box
            component='section'
            id={id}
            className={classes.root}
            {...sectionProps}
        >
            {overline && (
                <>
                    <Typography variant='subtitle1'>{overline}</Typography>
                    <Divider sx={{ margin: theme.spacing(1, 0), borderColor: theme.palette.divider }} />
                </>
            )}
            {heading && (
                <Typography variant='h2' sx={{ marginBottom: theme.spacing(2), ...(whiteHeading && { color: theme.palette.primary.contrastText }) }}>
                    <a href={`#${id}`}>
                        <Box
                            component='span'
                            sx={{
                                display: 'inline-block',
                                height:16,
                                width: 16,
                                backgroundColor: theme.palette.secondary.main,
                                borderRadius: 8,
                                verticalAlign: 'middle',
                                '&:hover': {
                                    backgroundColor: theme.palette.secondary.light,
                                },
                            }}
                        />
                    </a>
                &nbsp;
                    {heading}
                </Typography>
            )}
            {children}
        </Box>
    )
}) /* =====+======================================================================================================== */
