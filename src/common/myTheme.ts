/* eslint-disable import/no-relative-parent-imports */
import { Theme, darken, lighten, rgbToHex } from '@material-ui/core'
import { createTheme } from '@material-ui/core/styles'
import { BaseCSSProperties } from '@material-ui/styles'

import ChakraPetchBold from 'assets/fonts/ChakraPetch-Bold.ttf'
import ChakraPetchBoldItalic from 'assets/fonts/ChakraPetch-BoldItalic.ttf'
import ChakraPetchItalic from 'assets/fonts/ChakraPetch-Italic.ttf'
import ChakraPetchLight from 'assets/fonts/ChakraPetch-Light.ttf'
import ChakraPetchLightItalic from 'assets/fonts/ChakraPetch-LightItalic.ttf'
import ChakraPetchMedium from 'assets/fonts/ChakraPetch-Medium.ttf'
import ChakraPetchMediumItalic from 'assets/fonts/ChakraPetch-MediumItalic.ttf'
import ChakraPetchRegular from 'assets/fonts/ChakraPetch-Regular.ttf'
import ChakraPetchSemiBold from 'assets/fonts/ChakraPetch-SemiBold.ttf'
import ChakraPetchSemiBoldItalic from 'assets/fonts/ChakraPetch-SemiBoldItalic.ttf'
import OpenSansBold from 'assets/fonts/OpenSans-Bold.ttf'
import OpenSansBoldItalic from 'assets/fonts/OpenSans-BoldItalic.ttf'
import OpenSansExtraBold from 'assets/fonts/OpenSans-ExtraBold.ttf'
import OpenSansExtraBoldItalic from 'assets/fonts/OpenSans-ExtraBoldItalic.ttf'
import OpenSansItalic from 'assets/fonts/OpenSans-Italic.ttf'
import OpenSansLight from 'assets/fonts/OpenSans-Light.ttf'
import OpenSansLightItalic from 'assets/fonts/OpenSans-LightItalic.ttf'
import OpenSansRegular from 'assets/fonts/OpenSans-Regular.ttf'
import OpenSansSemiBold from 'assets/fonts/OpenSans-SemiBold.ttf'
import OpenSansSemiBoldItalic from 'assets/fonts/OpenSans-SemiBoldItalic.ttf'

import { foreground, foregroundShade } from './colors'


enum FONT_WEIGHT {
    LIGHT = 300,
    REGULAR = 400,
    // MEDIUM = 500,  // OpenSans doesn't have it.
    SEMIBOLD = 600,
    BOLD = 700,
    // EXTRABOLD = 800,  // ChakraPetch doesn't have it.
}

const ChakraPetchFontUrls = {
    'Bold': ChakraPetchBold,
    'BoldItalic': ChakraPetchBoldItalic,
    'Italic': ChakraPetchItalic,
    'Light': ChakraPetchLight,
    'LightItalic': ChakraPetchLightItalic,
    'Medium': ChakraPetchMedium,
    'MediumItalic': ChakraPetchMediumItalic,
    'Regular': ChakraPetchRegular,
    'SemiBold': ChakraPetchSemiBold,
    'SemiBoldItalic': ChakraPetchSemiBoldItalic,
}

const getChakraPetchSrc = (variant: keyof typeof ChakraPetchFontUrls) => {
    return `local('ChakraPetch'), local('ChakraPetch-${variant}'), url('${ChakraPetchFontUrls[variant]}')`
}

export const ChakraPetchFontFaces: BaseCSSProperties['@font-face'] = [
    { fontFamily: 'Chakra Petch', fontWeight: 300, fontStyle: 'italic', src: getChakraPetchSrc('LightItalic') },
    { fontFamily: 'Chakra Petch', fontWeight: 300, fontStyle: 'normal', src: getChakraPetchSrc('Light') },
    { fontFamily: 'Chakra Petch', fontWeight: 400, fontStyle: 'italic', src: getChakraPetchSrc('Italic') },
    { fontFamily: 'Chakra Petch', fontWeight: 400, fontStyle: 'normal', src: getChakraPetchSrc('Regular') },
    { fontFamily: 'Chakra Petch', fontWeight: 500, fontStyle: 'italic', src: getChakraPetchSrc('MediumItalic') },
    { fontFamily: 'Chakra Petch', fontWeight: 500, fontStyle: 'normal', src: getChakraPetchSrc('Medium') },
    { fontFamily: 'Chakra Petch', fontWeight: 600, fontStyle: 'italic', src: getChakraPetchSrc('SemiBoldItalic') },
    { fontFamily: 'Chakra Petch', fontWeight: 600, fontStyle: 'normal', src: getChakraPetchSrc('SemiBold') },
    { fontFamily: 'Chakra Petch', fontWeight: 700, fontStyle: 'italic', src: getChakraPetchSrc('BoldItalic') },
    { fontFamily: 'Chakra Petch', fontWeight: 700, fontStyle: 'normal', src: getChakraPetchSrc('Bold') },
]

const OpenSansFontUrls = {
    'Bold': OpenSansBold,
    'BoldItalic': OpenSansBoldItalic,
    'ExtraBold': OpenSansExtraBold,
    'ExtraBoldItalic': OpenSansExtraBoldItalic,
    'Italic': OpenSansItalic,
    'Light': OpenSansLight,
    'LightItalic': OpenSansLightItalic,
    'Regular': OpenSansRegular,
    'SemiBold': OpenSansSemiBold,
    'SemiBoldItalic': OpenSansSemiBoldItalic,
}

const getOpenSansSrc = (variant: keyof typeof OpenSansFontUrls) => {
    return `local('OpenSans'), local('OpenSans-${variant}'), url('${OpenSansFontUrls[variant]}')`
}

export const OpenSansFontFaces: BaseCSSProperties['@font-face'] = [
    { fontFamily: 'Open Sans', fontWeight: 300, fontStyle: 'italic', src: getOpenSansSrc('LightItalic') },
    { fontFamily: 'Open Sans', fontWeight: 300, fontStyle: 'normal', src: getOpenSansSrc('Light') },
    { fontFamily: 'Open Sans', fontWeight: 400, fontStyle: 'italic', src: getOpenSansSrc('Italic') },
    { fontFamily: 'Open Sans', fontWeight: 400, fontStyle: 'normal', src: getOpenSansSrc('Regular') },
    { fontFamily: 'Open Sans', fontWeight: 600, fontStyle: 'italic', src: getOpenSansSrc('SemiBoldItalic') },
    { fontFamily: 'Open Sans', fontWeight: 600, fontStyle: 'normal', src: getOpenSansSrc('SemiBold') },
    { fontFamily: 'Open Sans', fontWeight: 700, fontStyle: 'italic', src: getOpenSansSrc('BoldItalic') },
    { fontFamily: 'Open Sans', fontWeight: 700, fontStyle: 'normal', src: getOpenSansSrc('Bold') },
    { fontFamily: 'Open Sans', fontWeight: 800, fontStyle: 'italic', src: getOpenSansSrc('ExtraBoldItalic') },
    { fontFamily: 'Open Sans', fontWeight: 800, fontStyle: 'normal', src: getOpenSansSrc('ExtraBold') },
]

const cssifyFontFace = (fontFace: BaseCSSProperties['@font-face']): string => {
    if (!fontFace) return ''
    if (Array.isArray(fontFace)) return fontFace.map(cssifyFontFace).join('\n')

    return `
        @font-face {
            font-family: ${fontFace.fontFamily};
            font-style: ${fontFace.fontStyle};
            font-display: ${fontFace.fontDisplay ?? 'swap'};
            font-weight: ${fontFace.fontWeight};
            src: ${fontFace.src};
        }`
}

// Official Space Engineers colors (https://blog.marekrosa.org/2017/09/my-vision-for-visual-style-of-space.html)
export const SE_COLORS = {
    black: '#111111',
    grey: '#b6bab9',
    grey_dark: '#686868',
    white: '#efefef',

    red     : { main: '#c01118', dark: rgbToHex(darken('#c01118', 0.2)), light: rgbToHex(lighten('#c01118', 0.8)) },
    green   : { main: '#417e3b', dark: rgbToHex(darken('#417e3b', 0.2)), light: rgbToHex(lighten('#417e3b', 0.7)) },
    blue    : { main: '#1767ae', dark: rgbToHex(darken('#1767ae', 0.2)), light: rgbToHex(lighten('#1767ae', 0.7)) },
    yellow  : { main: '#f5bf2b', dark: rgbToHex(darken('#f5bf2b', 0.2)), light: rgbToHex(lighten('#f5bf2b', 0.4)) },
}

// declare module '@material-ui/core/styles/createTypography' {
//     interface Typography {
//         mono: TypographyStyle
//     }
//     interface TypographyOptions {
//         mono?: TypographyStyleOptions
//     }
// }

declare module '@material-ui/core/styles/createPalette' {
    interface SimplePaletteColorOptions {
        border?: string,
        background?: string,
    }
    interface PaletteColor {
        border: string,
        background: string,
    }
}

export type IMyTheme = Theme

/*
2020-05: https://gs.statcounter.com/screen-resolution-stats
- 10.31% - 360x640
- 8.94% - 1366x768
- 7.70% - 1920x1080
- 4.52% - 375x667
- 4.45% - 414x896
- 3.29%  - 1536x864
- next - 1600x900
*/

//  xs      sm       md       lg       xl
//  0px     600px    960px    1280px   1920px - default
//  0px     640px    960px    1280px   1920px - multiples of 640: 1, 1.5, 2, 3
//  0px     600px    900px    1200px   1800px - multiples of 600: 1, 1.5, 2, 3

// Analysis
// xs-sm: stretching half column
// sm-md: whole column
// md-lg: whole column
// lg-xl: two columns
// xl+++: three columns

// Compare
// xs-sm: "sorry, please use landscape"
// sm-md: half column + fullscreen drawer
// md-lg: half column + side drawer
// lg-xl: whole column
// xl+++: whole column


const SPACING = 8  // Default is 8.
const defaultTheme = createTheme({
    spacing: SPACING,
})

// To use the `typography.pxToRem()` helper.
const { typography: typographyRaw, palette: paletteRaw } = createTheme({
    typography: {
    // htmlFontSize: 16,  // 16 is default.
    },
    palette: {
        primary: {
            light: '#7BA45E',
            main: '#4F842A',
            dark: '#3E6325',
            contrastText: '#FFF',
            border: '#A7C194',  // 50% figma-ligther than main.
            background: '#EDF2E9',
        },
        secondary: {
            dark: '#6D1B7B',
            main: '#9C27B0',
            light: '#AF52BF',
            contrastText: '#FFF',
            border: '#CD93D7',  // 50% figma-ligther than main.
            background: '#FBF6FC',
        },
        background: {
            default: '#FFF',
            paper: '#F5F5F5',
        },
        text: {
            primary: SE_COLORS.black,
            secondary: SE_COLORS.grey_dark,
            disabled: SE_COLORS.grey,
        },
        divider: '#EEEEEE',
    },
})

// www.color-hex.com/color
export const MY_LIGHT_THEME: IMyTheme = createTheme({
    spacing: SPACING,
    palette: paletteRaw,

    typography: {
        fontFamily: '"Open Sans", "Roboto", sans-serif',
        fontWeightLight: FONT_WEIGHT.LIGHT,
        fontWeightRegular: FONT_WEIGHT.REGULAR,
        fontWeightMedium: FONT_WEIGHT.SEMIBOLD,
        fontWeightBold: FONT_WEIGHT.BOLD,

        h1: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(44),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.2,
            letterSpacing: '0em',
        },
        h2: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(34),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.2,
            letterSpacing: '0em',
        },
        h3: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(28),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.2,
            letterSpacing: '0em',
        },
        h4: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(24),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.25,
            letterSpacing: '0em',
        },
        h5: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(20),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.5,
            letterSpacing: '0em',
        },
        h6: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(16),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.5,
            letterSpacing: '0.1em',
            textTransform: 'uppercase',
        },

        subtitle1: {
            fontSize: typographyRaw.pxToRem(16),
            fontWeight: FONT_WEIGHT.BOLD,
            lineHeight: 1.25,
            letterSpacing: '0.05em',
            textTransform: 'uppercase',
        },
        subtitle2: {
            fontSize: typographyRaw.pxToRem(12),
            fontWeight: FONT_WEIGHT.REGULAR,
            lineHeight: 1.5,
            letterSpacing: '0.05em',
            textTransform: 'uppercase',
        },

        body1: {  // Also a label.
            fontSize: typographyRaw.pxToRem(16),
            fontWeight: FONT_WEIGHT.REGULAR,
            lineHeight: 1.5,
            letterSpacing: '0em',
        },
        body2: {
            fontSize: typographyRaw.pxToRem(16),
            fontWeight: FONT_WEIGHT.SEMIBOLD,
            lineHeight: 1.5,
            letterSpacing: '0.02em',  // Default 0.01071em
        },

        button: {
            fontFamily: '"Chakra Petch", "Roboto", sans-serif',
            fontSize: typographyRaw.pxToRem(14),
            fontWeight: FONT_WEIGHT.SEMIBOLD,
            lineHeight: 1.2,
            letterSpacing: '0.06em',
            textTransform: 'none',
        },

        caption: {
            fontSize: typographyRaw.pxToRem(12),
            fontWeight: FONT_WEIGHT.SEMIBOLD,
            lineHeight: 1.3,
            letterSpacing: '0.02em',
        },

        overline: {
            fontSize: typographyRaw.pxToRem(12),
            fontWeight: FONT_WEIGHT.REGULAR,
            lineHeight: 1.2,
            letterSpacing: '0.15em',
            textTransform: 'uppercase',
        },

        // // Custom style for monospace, e.g. raw filters textarea.
        // mono: {
        //     fontFamily: '"Roboto Mono", monospace',
        //     fontWeight: FONT_WEIGHT.REGULAR,
        //     fontSize: '0.75rem',  // 11px
        //     lineHeight: 1.2,
        //     letterSpacing: 0,
        // },
    },

    components: {
        MuiCssBaseline: {
            styleOverrides: 'html {scroll-behavior: auto;}' + cssifyFontFace([...ChakraPetchFontFaces, ...OpenSansFontFaces]),
        },
        MuiAccordion: {
            styleOverrides: {
                root: {
                    backgroundColor: 'unset',
                    '&.Mui-expanded': {
                        '&::before': {
                            opacity: 'unset',
                        },

                        flexDirection: 'row-reverse',
                        '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
                            transform: 'rotate(90deg)',
                        },
                    },
                },
            },
        },
        MuiAppBar: {
            defaultProps: {
                elevation: 0,
            },
            styleOverrides: {
                root: {
                    borderColor: paletteRaw.primary.border,
                    borderStyle: 'solid',
                    borderWidth: 0,
                    '&:first-of-type': {
                        borderBottomWidth: 1,
                    },
                    '&:last-of-type': {
                        borderTopWidth: 1,
                    },
                    flexDirection: 'row',  // why this is even 'column' by default??
                },
            },
        },
        MuiBottomNavigation: {
            styleOverrides: {
                root: {
                    backgroundColor: 'transparent',
                },
            },
        },
        MuiButton: {
            defaultProps: {
                disableElevation: true,
            },
            styleOverrides: {
                containedPrimary: {
                    backgroundColor: foregroundShade(paletteRaw.mode, paletteRaw.primary),
                    ':hover': {
                        backgroundColor: foreground(paletteRaw.mode, paletteRaw.primary.main),
                    },
                },
                outlined: {
                    borderWidth: 2,
                    '&:hover': {
                        borderWidth: 2,
                    },
                },
            },
        },
        MuiDivider: {
            styleOverrides: {
                root: {
                    borderColor: paletteRaw.primary.border,
                },
            },
        },
        MuiPaper: {
            defaultProps: {
                elevation: 0,
                square: true,
            },
        },
        MuiSvgIcon: {
            styleOverrides: {
                fontSizeLarge: {
                    fontSize: typographyRaw.pxToRem(36),  // Why default is 35?
                },
            },
        },
        MuiToolbar: {
            styleOverrides: {
                root: {
                    padding: 0,
                    minHeight: 52,
                    width: '100%',
                    [defaultTheme.breakpoints.up('sm')]: {
                        minHeight: 72,
                    },
                },
            },
        },
        MuiTableCell: {
            styleOverrides: {
                root: {
                    borderColor: paletteRaw.divider,
                    borderStyle: 'solid',
                    borderWidth: 1,
                    padding: defaultTheme.spacing(1, 1),
                    [defaultTheme.breakpoints.up('sm')]: {
                        padding: defaultTheme.spacing(1, 2),
                    },
                },
            },
        },
        MuiTableHead: {
            styleOverrides: {
                root: {
                    backgroundColor: paletteRaw.secondary.background,
                },
            },
        },
    },
})
