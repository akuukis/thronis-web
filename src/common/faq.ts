export interface FaqEntry {
    id: string,
    heading: string,
    details: string,

}

export const FAQ_ENTRIES: FaqEntry[] = [
    {
        id: 'panel1',
        heading: 'Kas ir Tronis?',
        details: 'Tronis ir vienas pilnas dienas spēle mežā, kur dalībnieki komandās cīnās par uzvaru. Spēle sastāv no cīņas par teritorijas kontroli, diplomātiju ar citām komandām, kaujām ar drošajiem zobeniem, ložņāšanas un slēpšanās, stratēģijas, politikas un pašu dalībnieku radītiem notikumiem.',
    },
    {
        id: 'panel2',
        heading: 'Vai es varu piedalīties?',
        details: 'Jā! Tronī var piedalīties jebkurš gribētājs neatkarīgi no iepriekšējas LARP pieredzes vai prasmēm. Vienīgi, ja esi nepilngadīgs, tev būs nepieciešama vecāku atļauja, kurā norādīts pilngadīgs spēles dalībnieks, kurš par tevi uzņemas atbildību.',
    },
    {
        id: 'panel3',
        heading: 'Ko man darīt, ja man nav komandas?',
        details: 'Tronis ir komandu spēle, kurā visu spēli tu kā savas komandas dalībnieks cīnīsies pret (un draudzēsies ar) citām komandām. Tas ir OK, ka neesi iepriekš spēlējis Troni un nevienu nepazīsti - tādus kā tu mēs ēdam brokastīs 🥐☕! Izrādi interesi spēlēt un orgi ar tevi aprunāsies un palīdzēs pievienoties piemērotākajai komandai. Vari uz spēli ierasties arī kā individuālais spēlētājs. Tad pēdējā brīdī pirms spēles sākuma pieliksim tevi komandai, kurai visvairāk nepieciešami papildus spēlētāji.',
    },
    {
        id: 'panel4',
        heading: 'Kad man ierasties?',
        details: 'Uz Troni vari ierasties gan piektdienas vakarā, gan sestdienas rītā. Vislabāk ierasties piektdienas vakarā, jo tad notiks lomu spēļu aktivitāte - Dzīres. Ja ierodies sestdien, paspēsi vien uz pašu spēli (kas ir pats galvenais, vai ne?). Uz Mazo Troni visi spēlētāji ierodas spēles rītā.',
    },
    {
        id: 'panel5',
        heading: 'Kā notiek pieteikšanās?',
        details: 'Dalībnieku pieteikšanās notiek caur Axelu. Spied pogu "Pieteikties", sazinies ar viņu un izpildi visu, kas prasīts reģistrācijā. Mēs iesakām pievienoties Discord serverim (saite kājenē), jo tad varēsi gan parunāt ar orgiem, gan atrast komandu, ja tev tādas vēl nav.',
    },
    {
        id: 'panel6',
        heading: 'Ko man vilkt mugurā?',
        details: 'Esi kādreiz skatījies Gredzenu pavēlnieku? Velc kaut ko tādā stilā. Vēlams kažokādas, dabīgi materiāli un drēbes un apavi bez spilgtiem uzrakstiem. Tas visiem palīzēs radīt viduslaicīgu noskaņu un smuki izskatīties bildēs.',
    },
    {
        id: 'panel7',
        heading: 'Ko mēs tur darīsim?',
        details: 'Vārtīsimies brūklenēs 🍓🍒... Bet, ja nopietni, Tronī tavs laiks paskries nemanot. Visu laiku skriesi savākt resursus no resursu nometnēm, iesi bariņā iekaustīt nodevējus kaimiņus vai dosies dzīrot pie draugu komandas. Ja nu kaut kad izdosies apsēsties, tad komandas bāzē pārrunāsi tālāko stratēģiju, palīdzēsi sakārtot ekonomiku, lai labāk ražojas uzvaras punkti, vai uzlabosi ekipējumu uz, piemēram, lielāku zobenu. Un vai es pieminēju, ka tavu vēderu uzpildīs siltas pusdienas?',
    },
    {
        id: 'panel8',
        heading: 'Kas jādara, lai pieteiktu savu komandu?',
        details: 'Lai pieteiktu komandu, jums jābūt vismaz 7 piereģistrētiem spēlētājiem (tevi ieskaitot). Kā komandai, jums jāizvēlas savs līderis jeb troņmantinieks, vēl neaizņemts dzīvnieks un krāsa (opcijas atradīsi Discord kanālā) un jāizveido karogs. Kad viss gatavs, uztaisiet mazu rakstiņu par to, kāpēc braucat uz karaļa Dzīrēm, kas jūs esat un kā plānojat spēlēt spēli. Kopā ar bildi, kurā ir jūsu karogs un pāris spēlētāji, atsūti rakstiņu Kalvim vai Rotai un viņi parūpēsies, lai visi zin, ka jūsu komanda pievienosies uz spēli.',
    },
    {
        id: 'panel9',
        heading: 'Vai varu līdz ņemt bērnus?',
        details: 'Diemžēl ņemt līdzi uz spēli bērnus nav atļauts. Tronis ir aktīvās atpūtas pasākums, kurā dalībnieki cīnās ar stilizētiem zobeniem. Ne  mēs, ne Tu nevar garantēt bērnu drošību brīdī, kad kāds vicinās ar zobenu auguma atšķirību dēļ. Un pašam arī nebūs tik interesanti, ja visu laiku jāpieskata sīkais! Toties pieņemam 16+ gadus vecus bērnus ar vecāku zīmi.',
    },

]
