import { useEffect, useState } from 'react'


export default () => {
    const [googlePad, setGooglePad] = useState(false)

    useEffect(() => {
        const interval = setInterval(() => {
            const googleToolbar = document.getElementById(':0.container') ?? document.getElementById(':1.container')
            const googleToolbarVisible = googleToolbar && googleToolbar?.parentElement?.style.display !== 'none'
            if (!googlePad && googleToolbarVisible) setGooglePad(true)
            if (googlePad && !googleToolbarVisible) setGooglePad(false)
        }, 1000)
        return () => clearInterval(interval)
    }, [googlePad])

    return googlePad
}
