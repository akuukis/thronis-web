import * as React from 'react'

import { Box, Button, Grid, Theme, Typography } from '@material-ui/core'
import { SxProps } from '@material-ui/system/styleFunctionSx/styleFunctionSx'
import { useNavigate } from 'react-router'

import { useBreakpoint } from 'src/common'
import { createFC } from 'src/common/createFC'

import { IClothes } from '../containers/Clothes/common'


interface ClothesCardProps {
    item: IClothes,
    sx?: SxProps<Theme>,
    sxCaption?: SxProps<Theme>,
    sxBox?: SxProps<Theme>,
    left?: boolean,
}

export const ClothesCard = createFC(import.meta.url)<ClothesCardProps>(({
    item,
    theme,
    sx = {},
    children,
    left = true,
    sxCaption = {},
    sxBox = {},
}) => {
    sx = { ...{}, ...sx }
    const navigate = useNavigate()
    const isLgUp = useBreakpoint('lg')

    sxCaption = { ...{ alignSelf: 'bottom' }, ...sxCaption }
    sxBox = { ...{ padding: isLgUp ? 4 : 0, display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100%', background: 'linear-gradient(154.17deg, #4F842A 0%, #7BA45E 83.69%)' }, ...sxBox }

    return (
        <Grid container spacing={isLgUp ? 0 : 2} sx={sx} direction={left ? 'row' : 'row-reverse'}>
            <Grid item xs={12} lg={8}>
                {item.heading && !isLgUp && (
                    <Typography variant='h3' sx={{ marginBottom: isLgUp ? 2 : 4 }}>
                        {item.heading}
                    </Typography>
                )}

                <Box
                    component="img"
                    src={item.image}
                    alt={item.caption}
                    sx={{ minHeight: 366, width: '100%', height: isLgUp ? '100%' : 'auto', aspectRatio: '4', objectFit: 'cover', objectPosition: 'center' }}
                />
                {item.caption && !isLgUp && (
                    <Box component='div' sx={{ textAlign: 'center' }}>
                        <Typography variant='caption' sx={{ color: theme.palette.text.primary }}>
                            {item.caption}
                        </Typography>
                    </Box>
                )}
            </Grid>
            <Grid item xs={12} lg={4}>
                <Box sx={sxBox}>
                    <Box textAlign={!left && isLgUp ? 'right' : 'left'}>
                        {item.heading && isLgUp && (
                            <Typography variant='h3' sx={{ marginBottom: isLgUp ? 2 : 4 }}>
                                {item.heading}
                            </Typography>
                        )}
                        {item.content ? item.content : children}
                        {item.button && item.buttonHref && (
                            <Typography sx={isLgUp ? {} : { textAlign: 'center' }}>
                                <Button
                                    variant={isLgUp ? 'outlined' : 'contained'}
                                    color={isLgUp ? 'inherit' : 'primary'}
                                    href={item.buttonHref}
                                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                    onClick={(e) => {e.preventDefault(); navigate(item.buttonHref!)}}
                                    sx={{ marginTop: 2.5 }}
                                >
                                    {item.button}
                                </Button>
                            </Typography>
                        )}
                    </Box>
                    {item.caption && isLgUp && (
                        <Typography variant='caption' sx={sxCaption} align={left ? 'left' : 'right'}>
                            {left && '← '} {item.caption} {!left && ' →'}
                        </Typography>
                    )}
                </Box>
            </Grid>
        </Grid>
    )
})
