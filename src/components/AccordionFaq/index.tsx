import React from 'react'

import { Accordion } from '@material-ui/core'
import { AccordionSummary } from '@material-ui/core'
import { AccordionDetails } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'

import { createFC } from 'src/common'
import { FaqEntry } from 'src/common/faq'


interface Props {
    faq: FaqEntry
    expanded: boolean
    onChange: () => void
}

export default createFC(import.meta.url)<Props>(({ children, theme, faq, expanded, onChange }) => {
    return (
        <Accordion disableGutters sx={{ color: '000000' }} expanded={expanded} onChange={onChange}>
            <AccordionSummary
                expandIcon={<ArrowForwardIosIcon fontSize='small' />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
            >
                <Typography variant='h6' sx={{ width: '70%', flexShrink: 0, lineHeight: 2.2 }}>
                    {faq.heading}
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography>{faq.details}</Typography>
            </AccordionDetails>
        </Accordion>
    )
})
