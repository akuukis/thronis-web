import * as React from 'react'

import { Link as LinkMaterial, LinkProps, Theme, Tooltip } from '@material-ui/core'
import { Typography } from '@material-ui/core/styles/createTypography'
import { CSSProperties } from '@material-ui/styles'
import classname from 'classnames'
import { Link as LinkRouter } from 'react-router-dom'

import { createFC, createStyles } from 'src/common'


const DUD_URL = 'javascript:;'

const styles = (theme: Theme) => createStyles({
    root: {
        color: ({ color }: { color?: LinkProps['color'] | 'contrastText' }) => {
            switch (color) {
                case 'inherit':
                    return 'inherit'
                case 'contrastText':
                    return theme.palette.primary.contrastText
                case 'primary':
                case 'secondary':
                case 'error':
                    return theme.palette.error.main
                case 'textPrimary':
                    return theme.palette.text.primary
                case 'textSecondary':
                    return theme.palette.text.secondary
                default:
                    return theme.palette.primary.dark
            }
        },
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'underline',
        },
    },
    router: {
    },
    material: {
    },
    noWrap: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
    },
})

type UniversalLinkProps = Omit<React.ComponentProps<'a'>, 'ref'> & {
    href: string;
    title?: string;
    variant?: LinkProps['variant'];
    noWrap?: LinkProps['noWrap'];
    blank?: boolean;
}
// TODO: Check why color is randomly ignored 
& Pick<LinkProps, 'color'>

/**
 * TODO: simplify using `<Link as={...}>{...}</Link>`
 */
export const UniversalLink = createFC(import.meta.url, styles)<UniversalLinkProps>(({ children, classes, theme, blank, href, noWrap, title, variant, color = 'secondary', ...otherProps }) => {
    let to: null | string = null

    function wrapInTooltip(jsx: JSX.Element) {
        if (!title) return jsx

        return (
            <Tooltip arrow placement='top' title={title}>
                {jsx}
            </Tooltip>
        )
    }

    try {
    // Error if not absolute. Also, if absolute but with matching origin, use it as relative instead (to avoid refresh).
        const url = new URL(href)
        if (url.origin === document.location.origin) to = href.slice(url.origin.length)
    } catch (err) {
    // If not absolute href then naively assume it's a relative path.
        to = href
    }

    if (to) {
    // Relateive path only. Will not refresh.
        return wrapInTooltip(
            <LinkRouter
                className={classname(classes.root, classes.router, noWrap && classes.noWrap)}
                to={to}
                // eslint-disable-next-line react/forbid-component-props
                style={variant ? theme.typography[variant as keyof Typography] as CSSProperties : {}}
                {...(blank ? { target: '_blank', rel: 'noreferrer noopener' } : {})}
                {...otherProps}
            >
                {children}
            </LinkRouter>,
        )
    } else {
    // Any path. Will always refresh.
        return wrapInTooltip(
            <LinkMaterial
                color={color}
                className={classname(classes.root, classes.material)}
                variant={variant}
                noWrap={noWrap}
                href={href}
                {...(href === DUD_URL ? {} : { target: '_blank', rel: 'noreferrer noopener' })}
                {...otherProps}
            >
                {children}
            </LinkMaterial>,
        )
    }

})
