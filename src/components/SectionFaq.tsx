import React, { useCallback } from 'react'

import { Button, Grid } from '@material-ui/core'
import { useNavigate } from 'react-router'

import { createFC, useBreakpoint } from 'src/common'
import { FAQ_ENTRIES } from 'src/common/faq'
import AccordionFaq from 'src/components/AccordionFaq'
import { ROUTE } from 'src/constants'
import Section from 'src/layouts/DefaultLayout/Section'


export default createFC(import.meta.url)(({ children, theme }) => {
    const lgUp = useBreakpoint('lg')
    const navigate = useNavigate()

    const [expanded, setExpanded] = React.useState<string | false>(false)
    const handleChange = useCallback((panel: string) => () => {
        setExpanded((expandedPrev) => expandedPrev !== panel ? panel : false)
    }, [])

    return (
        <Section overline="FAQ" heading="Biežāk uzdotie jautājumi">
            <Grid container spacing={lgUp ? 4 : 2} justifyContent='center'>
                <Grid item xs={12}>
                    {FAQ_ENTRIES.slice(0, 3).map((faq) => (
                        <AccordionFaq
                            key={faq.id}
                            faq={faq}
                            expanded={expanded === faq.id}
                            onChange={handleChange(faq.id)}
                        />
                    ))}
                </Grid>
                <Grid item>
                    <Button
                        variant='outlined'
                        color="primary"
                        href={ROUTE.FAQ}
                        onClick={(e) => {e.preventDefault(); navigate(ROUTE.FAQ)}}
                    >
                        uz FAQ
                    </Button>
                </Grid>
            </Grid>
        </Section>
    )
})
