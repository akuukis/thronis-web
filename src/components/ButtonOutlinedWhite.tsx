import { Button, Theme } from '@material-ui/core'
import { withStyles } from '@material-ui/styles'


export default withStyles((theme: Theme) => ({
    root: {
        borderColor: theme.palette.primary.background,
        color: theme.palette.primary.contrastText,
        '&:hover': {
            backgroundColor: theme.palette.primary.background,
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: theme.palette.text.disabled,
            borderColor: theme.palette.text.disabled,
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
}))(Button)
