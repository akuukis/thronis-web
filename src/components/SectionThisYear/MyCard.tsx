import * as React from 'react'

import { Card, CardActions, CardContent, CardProps, Typography } from '@material-ui/core'

import { createFC } from 'src/common/createFC'


interface Props extends CardProps {
    heading: string
    overline: string
    description: React.ReactNode
    button: React.ReactNode
}

export default createFC(import.meta.url)<Props>(({
    children,
    theme,
    heading,
    overline,
    description,
    button,
    ...cardProps
}) => {
    return (
        <Card
            variant="elevation"
            {...cardProps}
            sx={{
                [theme.breakpoints.down('md')]: {
                    margin: theme.spacing(0, -2),
                },
                ...cardProps?.sx,
            }}
        >
            <CardContent
                sx={{
                    padding: theme.spacing(2),
                    [theme.breakpoints.up('md')]: {
                        padding: theme.spacing(4, 4, 2, 4),
                    },
                }}
            >
                <Typography variant="subtitle1" gutterBottom>{overline}</Typography>
                <Typography variant="h3" gutterBottom>{heading}</Typography>
                <Typography paragraph>{description}</Typography>
                {children}
            </CardContent>
            <CardActions sx={{ justifyContent:'center', padding: theme.spacing(0, 4, 4, 4) }}>
                {button}
            </CardActions>
        </Card>
    )
})
