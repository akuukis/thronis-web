import React from 'react'

import { Button, Grid } from '@material-ui/core'
import { Box } from '@material-ui/system'
import { useNavigate } from 'react-router'

import emoji_swords from 'assets/events/emoji_swords.png'
import rightimg from 'assets/winner_team.jpg'

import { createFC, useBreakpoint } from 'src/common'
import { ItemCard } from 'src/components/ItemCard'
import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'
import Section from 'src/layouts/DefaultLayout/Section'

import RuleCards from '../RuleCards'
import { RULE_GAMES } from '../RuleCards/common'


import MyCard from './MyCard'
import SecondCard from './SecondCard'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const lgUp = useBreakpoint('lg')
    const mdUp = useBreakpoint('md')
    const navigate = useNavigate()

    return (
        <Section heading='Svarīgi zināt - TRONIS 2024 - ATCELTS' overline='Tronis šogad'>
            <Grid container spacing={lgUp ? 4 : 2} justifyContent='center'>
                <Grid item xs={12} lg={6}>
                    <MyCard
                        overline='2025'
                        heading='Tronis XI'
                        description='Uzzini visu svarīgāko par spēli šajā vasarā.'
                        button={<Button variant='contained' color='secondary' onClick={(e) => {e.preventDefault(); navigate(ROUTE.VENUE)}}>par Troni X</Button>}
                    >
                        <Box component='img' src={rightimg} sx={{ objectFit: 'cover', width: '100%', height: 400 }} />
                    </MyCard>
                </Grid>
                <Grid item xs={12} lg={6}>
                    <MyCard
                        overline='Noteikumi'
                        heading='Izlasi pirms spēles'
                        description='Iepazīsties ar Tronis XI spēles noteikumiem (pagājušā gada noteikumi, bet lielas izmaiņas nav plānotas).'
                        button={<Button variant='contained' color='primary' onClick={(e) => {e.preventDefault(); navigate(ROUTE.RULES)}}>pilnie spēles noteikumi</Button>}
                        sx={{ backgroundColor: 'transparent' }}
                    >
                        <RuleCards items={RULE_GAMES[0].files} placement="highlight" />
                    </MyCard>
                    <SecondCard
                        sx={{ backgroundColor: 'transparent' }}
                        overline='Pasākumu kalendārs'
                        heading='Zobencīņu treniņi'                       
                        description='Līdz spēlei darba dienu vakaros notieks zobencīņu treniņi. Nāc, iemēģini roku un noķer Troņa sajūtu jau laicīgi! '
                    >
                        <Grid container spacing={mdUp ? 4 : 2} justifyContent='center'>
                            <Grid item xs={12} sm={6}>
                                <UniversalLink href={ROUTE.EVENTS} onClick={(e) => {e.preventDefault(); navigate(ROUTE.EVENTS)}}>
                                    <ItemCard
                                        sx={{ width: '100%', backgroundColor: theme.palette.background.default }}
                                        item={{ heading: 'Zobencīņu treniņi', image: emoji_swords, paragraph: '' }}
                                    />
                                </UniversalLink>
                            </Grid>
                        </Grid>
                    </SecondCard>
                </Grid>
            </Grid>
        </Section>
    )
}) /* =============================================================================================================== */
