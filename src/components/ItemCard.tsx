import * as React from 'react'

import {
    Box,
    Card,
    CardContent, Theme, Typography, darken,
} from '@material-ui/core'
import { CSSProperties } from '@material-ui/system'
import { SxProps } from '@material-ui/system/styleFunctionSx/styleFunctionSx'


import { createFC } from 'src/common/createFC'


export interface IItem {
    image: string,
    heading: string,
    paragraph: React.ReactNode,
}

interface ItemCardProps {
    item: IItem,
    sx?: SxProps<Theme>,
}

export const ItemCard = createFC(import.meta.url)<ItemCardProps>(({
    item,
    theme,
    sx = {},
}) => {
    sx = { ...{
        'borderColor': theme.palette.primary.border,
        'background': theme.palette.background.default,
        'height' : '100%',
        'display' : 'flex',
        'justifyContent' : 'center',
        'alignItems' : 'center',
        '&:hover': {
            backgroundColor: darken(theme.palette.background.paper, 0.1),
        },
    } as CSSProperties, ...sx }

    return (
        <Card
            variant="outlined"
            square={false}
            sx={sx}
        >
            <CardContent>
                <Typography align='center'><Box component='img' src={item.image} alt={item.heading} sx={{ height: 36 }} /></Typography>
                <Typography align='center' variant="h4">{item.heading}</Typography>
                {item.paragraph && <Typography align='center' sx={{ marginTop: 2 }}>{item.paragraph}</Typography>}
            </CardContent>
        </Card>
    )
})
