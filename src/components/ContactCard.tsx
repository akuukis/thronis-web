import * as React from 'react'

import {
    Card,
    CardContent,
    CardMedia,
    Grid,
    Theme,
    Typography,
} from '@material-ui/core'

import { createFC, createStyles } from 'src/common'
import { IContact } from 'src/containers/Contacts/common'

import IconDiscord from './icons/IconDiscord'
import IconEmail from './icons/IconEmail'
import IconFacebook from './icons/IconFacebook'
import IconPhone from './icons/IconPhone'
import { UniversalLink } from './UniversalLink'


const styles = ((theme: Theme) => createStyles({
    textRow: {
        display: 'flex',
        height: 30,
        alignItems: 'center',
    },
    cover: {
        width: 120,
        height: 120,
    },
    paddingTop: {
        paddingTop: theme.spacing(2),
    },
    card: {
        borderColor: theme.palette.primary.border,
        background: theme.palette.background.default,
    },
    textLeftSpace: {
        paddingLeft: theme.spacing(0.5),
    },
}))

type ContactCardProps = Omit<React.ComponentProps<'a'>, 'ref'> & {
    contact: IContact;
}

export const ContactCard = createFC(import.meta.url, styles)<ContactCardProps>(({ classes, contact }) => {
    return (
        <Card variant="outlined" square={false} className={classes.card}>
            <CardContent>
                <Typography variant="subtitle1" color="textSecondary">
                    {contact.position}
                </Typography>
                <Typography variant="h2">
                    {contact.fullName}
                </Typography>
                <Grid container spacing={1} className={classes.paddingTop}>
                    <Grid item>
                        <CardMedia
                            className={classes.cover}
                            image={contact.image}
                            title={contact.fullName}
                        />
                    </Grid>
                    <Grid item>
                        <Grid container spacing={0} direction="column" height="100%">
                            {contact.phone && (
                                <Grid item className={classes.textRow}>
                                    <IconPhone />
                                    &nbsp;
                                    <UniversalLink href={`tel:${contact.phone}`}>
                                        <Typography className={classes.textLeftSpace}>{contact.phone}</Typography>
                                    </UniversalLink>
                                </Grid>
                            )}
                            {contact.discord && (
                                <Grid item className={classes.textRow}>
                                    <IconDiscord />
                                    &nbsp;
                                    <Typography className={classes.textLeftSpace}>{contact.discord}</Typography>
                                </Grid>
                            )}
                            {contact.facebook && (
                                <Grid item className={classes.textRow}>
                                    <IconFacebook />
                                    &nbsp;
                                    <UniversalLink href={`https://facebook.com/${contact.facebook}`}>
                                        <Typography className={classes.textLeftSpace}>{contact.facebook}</Typography>
                                    </UniversalLink>
                                </Grid>
                            )}
                            {contact.email && (
                                <Grid item className={classes.textRow}>
                                    <IconEmail />
                                    &nbsp;
                                    <UniversalLink href={`mailto:${contact.email}`}>
                                        <Typography className={classes.textLeftSpace}>{contact.email}</Typography>
                                    </UniversalLink>
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    )
})
