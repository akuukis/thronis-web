import * as React from 'react'

import {
    Card,
    CardContent, Typography,
} from '@material-ui/core'

import { createFC } from 'src/common/createFC'


export interface IEvents {
    heading: string,
    date?: string,
    time: string,
    paragraph: string,
}
interface EventCardProps {
    event: IEvents;
}

export const EventCard = createFC(import.meta.url)<EventCardProps>(({
    event,
    theme,
}) => {
    return (
        <Card
            variant="outlined"
            square={false}
            sx={{
                borderColor: theme.palette.secondary.border,
                background: theme.palette.background.default,
            }}
        >
            <CardContent>
                <Typography variant="h3" sx={{ marginBottom: 2 }}>{event.heading}</Typography>
                {event.date ?? <Typography variant="subtitle1">{event.date}</Typography>}
                <Typography variant="subtitle1">🕗&nbsp;&nbsp;{event.time}</Typography>
                <Typography paragraph sx={{ marginTop: 2, marginBottom: 0 }}>{event.paragraph}</Typography>
            </CardContent>
        </Card>
    )
})
