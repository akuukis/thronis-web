import React from 'react'

import { Grid, Paper, darken } from '@material-ui/core'

import brain_games from 'assets/sponsors/brain_games.png'
import kebabs from 'assets/sponsors/kebabs.png'
import kerlings from 'assets/sponsors/kerlinga_halle.png'
import laserdog from 'assets/sponsors/laserdog.png'
import otra_elpa from 'assets/sponsors/otra_elpa.png'
import redbull from 'assets/sponsors/redbull.png'

import { createFC, useBreakpoint } from 'src/common'
import { UniversalLink } from 'src/components/UniversalLink'
import Section from 'src/layouts/DefaultLayout/Section'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const lgUp = useBreakpoint('lg')

    return (
        <Section heading='Mūs atbalsta' overline='sponsori'>
            <Grid container spacing={lgUp ? 4 : 2} justifyContent='center'>
                {SPONSORS.map(({ image, alt, href }) => (
                    <Grid key={alt} item>
                        <UniversalLink href={href} target="_blank">
                            <Paper
                                sx={{
                                    'background': theme.palette.background.paper,
                                    'height' : theme.spacing(16),
                                    'width' : theme.spacing(16),
                                    [theme.breakpoints.up('md')]: {
                                        'height' : theme.spacing(30),
                                        'width' : theme.spacing(30),
                                        'padding' : theme.spacing(6),
                                    },
                                    'display' : 'flex',
                                    'justifyContent' : 'center',
                                    'alignItems' : 'center',
                                    'padding' : theme.spacing(2),
                                    '&:hover': {
                                        backgroundColor: darken(theme.palette.background.paper, 0.1),
                                    } }}
                            >
                                <img
                                    width="100%"
                                    alt={alt}
                                    src={image}
                                />
                            </Paper>
                        </UniversalLink>
                    </Grid>
                ))}
            </Grid>
        </Section>
    )
}) /* =============================================================================================================== */

const SPONSORS = [
    { image: redbull     , alt: 'Red Bull'        , href: 'https://www.redbull.com/lv-lv?utm_source=tronis' },
    { image: otra_elpa   , alt: 'Otrā Elpa'       , href: 'https://www.otraelpa.lv?utm_source=tronis' },
    { image: brain_games , alt: 'Brain Games'     , href: 'https://www.brain-games.lv?utm_source=tronis' },
    { image: kebabs      , alt: 'Siguldas kebabs' , href: 'https://www.facebook.com/SiguldasKebabs?utm_source=tronis' },
    { image: laserdog    , alt: 'Laserdog'        , href: 'https://www.laserdog.lv?utm_source=tronis' },
    { image: kerlings    , alt: 'Kērlinga halle'  , href: 'http://kerlingahalle.lv?utm_source=tronis' },
]
