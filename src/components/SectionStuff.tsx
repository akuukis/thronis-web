import React from 'react'

import { Button, Grid } from '@material-ui/core'
import { useNavigate } from 'react-router'

import emoji_clothes from 'assets/clothes/emoji_clothes.png'
import emoji_swords from 'assets/events/emoji_swords.png'
import tent from 'assets/items/tent.png'

import { createFC, useBreakpoint } from 'src/common'
import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'
import Section from 'src/layouts/DefaultLayout/Section'

import { ItemCard } from './ItemCard'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const lgUp = useBreakpoint('lg')
    const navigate = useNavigate()

    return (
        <Section heading='Ko ņemt līdzi?' overline='mantas'>
            <Grid container spacing={lgUp ? 4 : 2} justifyContent='center'>
                <Grid item xs={12} sm={4}>
                    <UniversalLink href={ROUTE.EQUIPMENT} onClick={(e) => {e.preventDefault(); navigate(ROUTE.EQUIPMENT)}}>
                        <ItemCard
                            sx={{ width: '100%', borderColor: theme.palette.primary.border, backgroundColor: theme.palette.background.paper }}
                            item={{ heading: 'Ekipējums', image: emoji_swords, paragraph: '' }}
                        />
                    </UniversalLink>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <UniversalLink href={ROUTE.CLOTHES} onClick={(e) => {e.preventDefault(); navigate(ROUTE.CLOTHES)}}>
                        <ItemCard
                            sx={{ width: '100%', borderColor: theme.palette.primary.border, backgroundColor: theme.palette.background.paper }}
                            item={{ heading: 'Apģērbs', image: emoji_clothes, paragraph: '' }}
                        />
                    </UniversalLink>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <UniversalLink href={ROUTE.ITEMS} onClick={(e) => {e.preventDefault(); navigate(ROUTE.ITEMS)}}>
                        <ItemCard
                            sx={{ width: '100%', borderColor: theme.palette.primary.border, backgroundColor: theme.palette.background.paper }}
                            item={{ heading: 'Mantas', image: tent, paragraph: '' }}
                        />
                    </UniversalLink>
                </Grid>
                <Grid item>
                    <Button
                        variant='outlined'
                        color="primary"
                        href={ROUTE.ITEMS}
                        onClick={(e) => {e.preventDefault(); navigate(ROUTE.ITEMS)}}
                    >
                        Vairāk par mantām
                    </Button>
                </Grid>
            </Grid>
        </Section>
    )
}) /* =============================================================================================================== */
