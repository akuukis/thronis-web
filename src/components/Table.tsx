import * as React from 'react'

import { TableBody, TableCell, TableHead, TableRow, Table as TableUi, Theme, Typography } from '@material-ui/core'
import { SxProps } from '@material-ui/system'

import { createFC } from 'src/common/createFC'


export interface TableProps {
    columns: string[],
    rows: string[][],
    sx?: SxProps<Theme>,
    bold?: boolean
}

export const Table = createFC(import.meta.url)<TableProps>(({ theme, columns, rows, sx, bold }) => {
    return (
        <TableUi sx={sx}>
            <TableHead>
                <TableRow sx={{
                    background: theme.palette.secondary.background,
                    borderColor: theme.palette.secondary.background,
                    borderWidth: 1,
                    borderStyle: 'solid',
                }}
                >
                    {Object.values(columns).map((value) => <TableCell><Typography align="center" variant="h6">{value}</Typography></TableCell> )}
                </TableRow>
            </TableHead>
            <TableBody sx={{ background: theme.palette.background.default }}>
                {Object.values(rows).map((row) => {
                    return (
                        <TableRow sx={{ border: '1px solid #EEEEEE' }}>
                            {Object.values(row).map((value, index) => <TableCell><Typography sx={ bold && index % 2 ? { fontWeight: 'bold' } : {}} align="center" variant="body1">{value}</Typography></TableCell> )}
                        </TableRow>
                    )
                })}
            </TableBody>
        </TableUi>
    )
})
