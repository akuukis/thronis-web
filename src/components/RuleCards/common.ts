import summaryMini2020 from 'assets/rules/Mini-Throne-2020-Rules-summary.pdf'
import appendixMini2022 from 'assets/rules/Mini-Throne-2022-Rules-appendix.pdf'
import summaryMini2022 from 'assets/rules/Mini-Throne-2022-Rules-summary.pdf'
import rulesMini2022 from 'assets/rules/Mini-Throne-2022-Rules.pdf'
import summaryIX from 'assets/rules/Throne-IX-Rules-summary.pdf'
import summaryVI from 'assets/rules/Throne-VI-Rules-summary.pdf'
import summaryVII from 'assets/rules/Throne-VII-Rules-summary.pdf'
import summaryVIII from 'assets/rules/Throne-VIII-Rules-summary.pdf'
import summaryX from 'assets/rules/Throne-X-Rules-summary.pdf'


export interface IRule {
    type: 'pdf' | 'doc'
    text: string,
    title: string,
    link: string,
}

export interface IRuleGame{
    title: string,
    files: IRule[],
}

export const RULE_GAMES: IRuleGame[] = [
    { title: 'Throne XI', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryX },
        { title:'Rules', text: '13 pages', type: 'doc', link: 'https://docs.google.com/document/d/12ZNhbDWVsD64RpiBdzELCpbOoPev5p66HBHEPVxv2A4' },
        { title:'Appendix', text: '4 pages', type: 'doc', link: 'https://docs.google.com/document/d/12ZNhbDWVsD64RpiBdzELCpbOoPev5p66HBHEPVxv2A4' },
    ] },
    { title: 'Throne X', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryX },
        { title:'Rules', text: '13 pages', type: 'doc', link: 'https://docs.google.com/document/d/1YZt7rbMbDvEtt1RbljeCEMm0RueafC5iLhnhhe0dAa4' },
        { title:'Appendix', text: '4 pages', type: 'doc', link: 'https://docs.google.com/document/d/1mMVEud3rx6u2LAyxQXpqT2Vffy-mm2iF9BOY1z6E2HA' },
    ] },
    { title: 'Throne IX', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryIX },
        { title:'Rules', text: '13 pages', type: 'doc', link: 'https://docs.google.com/document/d/17LtNHv8Y93gtI1ro1X9iYK4i-0JxyToJ1TLLwcBj_WE' },
        { title:'Appendix', text: '4 pages', type: 'doc', link: 'https://docs.google.com/document/d/1fN5T2kN-QSmEKf0XRzzWqtQnsc9-6Rey_IOC9S4j1RA' },
    ] },
    { title: 'mini Throne 2022', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryMini2022 },
        { title:'Rules', text: '13 pages', type: 'pdf', link: rulesMini2022 },
        { title:'Appendix', text: '4 pages', type: 'pdf', link: appendixMini2022 },
    ] },
    { title: 'Throne VIII', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryVIII },
        { title:'Rules', text: '15 pages', type: 'doc', link: 'https://docs.google.com/document/d/1ukGUhEKDkZlsuMSWkWbZNW1XI4e6Iqk8FPLOk4lCVs4/edit?usp=sharing' },
        { title:'Appendix', text: '5 pages', type: 'doc', link: 'https://docs.google.com/document/d/14PlP9bLOOMfQAruHgBX0mmA6nEFH0ZMgt5eGfsLhOSQ/edit?usp=sharing' },
    ] },
    { title: 'Throne VII', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryVII },
        { title:'Rules', text: '17 pages', type: 'doc', link: 'https://docs.google.com/document/d/1AR7XkCOmonvCtFpvs49QF8f2bb2EljprXsaa9hM_vIM/edit?usp=sharing' },
    ] },
    { title: 'mini Throne 2020', files:[
        { title:'Summary', text: '2 pages', type: 'pdf', link: summaryMini2020 },
        { title:'Rules', text: '10 pages', type: 'doc', link: 'https://docs.google.com/document/d/1r6FcA2zGP2KLFnOX-a-Gx9H-FxVxpQEM4p5RfkGciZw' },
    ] },
    { title: 'Throne VI', files:[
        { title:'Summary', text: '1 page', type: 'pdf', link: summaryVI },
        { title:'Rules', text: '13 pages', type: 'doc', link: 'https://docs.google.com/document/d/1eoO6QRpMofE8VwqW_bGvzgRVu8QiRju__2SER2flJpA/edit?usp=sharing' },
    ] },
    { title: 'Throne V', files:[
        { title:'Rules', text: '11 pages', type: 'doc', link: 'https://docs.google.com/document/d/1fGylAzfZLNdTCoKIKaRIuA446LZwa3hcszj2LE5UYVw/edit?usp=sharing' },
    ] },
]
