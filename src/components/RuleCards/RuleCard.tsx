import * as React from 'react'

import { Card, CardContent, Typography } from '@material-ui/core'
import IconDocument from '@material-ui/icons/Article'
import IconPDF from '@material-ui/icons/PictureAsPdf'

import { createFC } from 'src/common/createFC'
import { UniversalLink } from 'src/components/UniversalLink'

import { IRule } from './common'


interface Props {
    item: IRule,
    variant: 'accent' | 'default' | 'grey'
}

export default createFC(import.meta.url)<Props>(({ theme, item, variant }) => {
    return (
        <UniversalLink target='_blank' href={item.link}>
            <Card
                variant="outlined"
                square={false}
                sx={{
                    'borderColor': variant !== 'grey' ? theme.palette.primary.main : theme.palette.text.disabled,
                    'background': variant !== 'accent' ? theme.palette.background.default : theme.palette.primary.background,
                    'height' : '100%',
                    'display' : 'flex',
                    'justifyContent' : 'center',
                    'alignItems' : 'center',
                    padding: 1.5,
                }}
            >
                <CardContent sx={{ padding: '0 !important' }}>
                    <Typography align='center' variant='h6'>{item.title}</Typography>
                    <Typography align='center'>{item.text}</Typography>
                    <Typography align='center' sx={{ paddingTop: 1 }}>
                        {item.type === 'pdf' ? (
                            <IconPDF color={variant !== 'grey' ? 'primary' : 'disabled'} fontSize='large' />
                        ) : (
                            <IconDocument color={variant !== 'grey' ? 'primary' : 'disabled'} fontSize='large' />
                        )}
                    </Typography>
                </CardContent>
            </Card>
        </UniversalLink>

    )
})
