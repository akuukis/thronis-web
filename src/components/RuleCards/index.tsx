import React from 'react'

import { Grid } from '@material-ui/core'

import { createFC } from 'src/common'

import { IRule } from './common'
import RuleCard from './RuleCard'


interface Props {
    items: IRule[],
    placement: 'highlight' | 'history'
}

export default createFC(import.meta.url)<Props>(({ children, theme, items , placement }) => {

    return (
        <Grid container spacing={2}>
            {items.map((file, i, array) => (
                <Grid item key={file.title} xs={(i === 0 && array.length % 2 === 1 ) ? 12 : 6} sm={12/array.length as 12|6|4|3}>
                    <RuleCard item={file} variant={placement === 'highlight' ? (i===0 ? 'accent' : 'default') : 'grey'} />
                </Grid>
            ))}
        </Grid>
    )
}) /* =============================================================================================================== */
