import React from 'react'

import { createSvgIconFC } from 'src/common'


export default createSvgIconFC(<>
    <svg width="36" height="36" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg">
        <path d="M36.5 18C36.5 8.064 28.436 0 18.5 0C8.564 0 0.5 8.064 0.5 18C0.5 26.712 6.692 33.966 14.9 35.64V23.4H11.3V18H14.9V13.5C14.9 10.026 17.726 7.2 21.2 7.2H25.7V12.6H22.1C21.11 12.6 20.3 13.41 20.3 14.4V18H25.7V23.4H20.3V35.91C29.39 35.01 36.5 27.342 36.5 18Z" fill="currentColor" />
    </svg>

</>, { viewBox: '0 0 36 36' })
