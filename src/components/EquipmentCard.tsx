import * as React from 'react'

import { Box, Button, Grid, Theme, Typography } from '@material-ui/core'
import { ArrowForward } from '@material-ui/icons'
import { SxProps } from '@material-ui/system/styleFunctionSx/styleFunctionSx'
import { useNavigate } from 'react-router'

import { useBreakpoint } from 'src/common'
import { createFC } from 'src/common/createFC'
import { Table } from 'src/components/Table'

import { IEquipment } from '../containers/Equipment/common'


interface EquipmentCardProps {
    item: IEquipment,
    sx?: SxProps<Theme>,
    chUp?: boolean
}

export const EquipmentCard = createFC(import.meta.url)<EquipmentCardProps>(({
    item,
    theme,
    sx = {},
    children,
    chUp = true,
}) => {
    const isLgUp = useBreakpoint('lg')
    const navigate = useNavigate()

    sx = { ...{
        background: theme.palette.background.default,
        padding: isLgUp ? 4 : 0,
        paddingBottom: 4,
        paddingTop: 4,
    }, ...sx }

    sx = isLgUp ? sx : { ...sx, ...{ background: theme.palette.background.default } }

    return (
        <Box component='div' sx={sx}>
            <Grid container spacing={4}>
                <Grid item xs={12} lg={6}>
                    <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100%' }}>
                        <Box>
                            {item.heading && (
                                <Typography variant='h3'>
                                    {item.heading}
                                </Typography>
                            )}
                            {(chUp || isLgUp) && (item.content || children)}
                            {item.button && item.href && isLgUp && (
                                <Button
                                    color='secondary'
                                    href={item.href}
                                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                    onClick={(e) => {e.preventDefault(); navigate(item.href!)}}
                                    variant='contained'
                                    sx={{ marginTop: 4 }}
                                >
                                    {item.button}
                                </Button>
                            )}
                        </Box>
                        {item.caption && isLgUp && (
                            <Typography variant='caption'>
                                {item.caption}
                                <ArrowForward fontSize='inherit' sx={{ marginLeft: 1, verticalAlign: 'middle' }} />
                            </Typography>
                        )}
                    </Box>
                </Grid>
                <Grid item xs={12} lg={6}>
                    <Box component='img' src={item.image} sx={{ maxHeight: 200, maxWidth: '100%', display: 'flex', margin: '0 auto', objectFit: 'contain' }} />
                    {item.table && (
                        <Table columns={item.table.columns} rows={item.table.rows} />
                    )}
                    {!chUp && !isLgUp && (
                        item.content || children
                    )}
                    {item.button && item.href && !isLgUp && (
                        <Typography align='center'>
                            <Button
                                color='secondary'
                                href={item.href}
                                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                onClick={(e) => {e.preventDefault(); navigate(item.href!)}}
                                variant='contained'
                                sx={{ marginTop: 4 }}
                            >
                                {item.button}
                            </Button>
                        </Typography>
                    )}
                    {item.caption && !isLgUp && (
                        <Box component='div'>
                            <Typography variant='caption'>
                                {item.caption}
                                <ArrowForward fontSize='inherit' sx={{ marginLeft: 1, verticalAlign: 'middle' }} />
                            </Typography>
                        </Box>
                    )}
                </Grid>
            </Grid>
        </Box>
    )
})
