import React from 'react'

import { Theme, createStyles } from '@material-ui/core'
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox'

import { createFC } from 'src/common'


const styles = ((theme: Theme) => createStyles({
    root: {
        color: theme.palette.primary.contrastText,
    },
}))

export const CheckboxWhite = createFC(import.meta.url, styles)<CheckboxProps>((props) => {
    return (
        <Checkbox
            className={props && props.classes && props.classes.root}
            disableRipple
            color="default"
            inputProps={{ 'aria-label': 'decorative checkbox' }}
            {...props}
        />
    )
})
