import { useEffect } from 'react'

import { useLocation } from 'react-router-dom'


import { IMyTheme, createFC, createStyles } from 'src/common'


const styles = (theme: IMyTheme) => createStyles({
    root: {},
})


interface IProps {
}


export default createFC(import.meta.url, styles)<IProps>(({ children, classes, theme }) => {
    const { pathname } = useLocation()

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [pathname])

    return null
}) /* ============================================================================================================== */
