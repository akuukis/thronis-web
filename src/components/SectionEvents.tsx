import React from 'react'

import { Button, Grid, Typography } from '@material-ui/core'
import { useNavigate } from 'react-router'

import emoji_party from 'assets/events/emoji_party.png'
import emoji_swords from 'assets/events/emoji_swords.png'

import { createFC, useBreakpoint } from 'src/common'
import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'
import Section from 'src/layouts/DefaultLayout/Section'

import { ItemCard } from './ItemCard'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const mdUp = useBreakpoint('md')
    const navigate = useNavigate()

    return (
        <Section heading='Pasākumi katru nedēļu' overline='pasākumu kalendārs'>
            <Typography paragraph>Sešas nedēļas līdz spēlei darba dienu vakaros notiek pirms-spēles Troņa pasākumi. Nāc, iemēģini roku un noķer Troņa sajūtu jau laicīgi!</Typography>
            <Grid container spacing={mdUp ? 4 : 2} justifyContent='center'>
                <Grid item xs={12} sm={6}>
                    <UniversalLink href={ROUTE.EVENTS} onClick={(e) => {e.preventDefault(); navigate(ROUTE.EVENTS)}}>
                        <ItemCard
                            sx={{ width: '100%', backgroundColor: theme.palette.background.paper }}
                            item={{ heading: 'Zobencīņu treniņi', image: emoji_swords, paragraph: '' }}
                        />
                    </UniversalLink>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <UniversalLink href={ROUTE.EVENTS} onClick={(e) => {e.preventDefault(); navigate(ROUTE.EVENTS)}}>
                        <ItemCard
                            sx={{ width: '100%', backgroundColor: theme.palette.background.paper }}
                            item={{ heading: 'Pasēdēšanas', image: emoji_party, paragraph: '' }}
                        />
                    </UniversalLink>
                </Grid>
                <Grid item>
                    <Button
                        variant='outlined'
                        color="secondary"
                        href={ROUTE.EVENTS}
                        onClick={(e) => {e.preventDefault(); navigate(ROUTE.EVENTS)}}
                    >
                        Uz visiem pasākumiem
                    </Button>
                </Grid>
            </Grid>
        </Section>
    )
}) /* =============================================================================================================== */
