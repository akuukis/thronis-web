import React from 'react'

import { Grid } from '@material-ui/core'
import { Typography } from '@material-ui/core'

import { createFC } from 'src/common'
import RuleCards from 'src/components/RuleCards'
import { RULE_GAMES } from 'src/components/RuleCards/common'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

// import summary from './Mini Throne 2022 - Rules (summary).pdf'


export default createFC(import.meta.url)(({ children, theme }) => {

    return (
        <Container
            title="Noteikumi"
            description="Šie ir pilnie spēles noteikumi. Noteikumi veidoti angļu valodā, lai tos varētu izmantot visi dalībnieki neatkarīgi no dzimtās valodas."
        >
            <Section heading="Rules Throne XI">
                <Grid container spacing={3}>
                    <Grid item xs={12} lg={6}>
                        <Typography>
                            Not everyone needs to read all the rules, but at least one person in each team needs to study them well.
                            Regardless, <strong>all players need to read at least the rules summary.</strong>  Current rules status: content is finished, proofread in progress.
                        </Typography>
                        <Typography>
                            No drastic changes are made from last year. The biggest changes - simplified Workshops and Industrial pillar.
                        </Typography>
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <RuleCards items={RULE_GAMES[0].files} placement='highlight' />
                    </Grid>
                </Grid>
            </Section>
            <Section heading="Preface">
                <Typography paragraph>
                    Before diving into the full ruleset, here's a rough guideline for navigating the rules:
                </Typography>
                <Typography paragraph>
                    As a Player, there are <strong>3 unofficial playstyles</strong> with fuzzy borders that you could take upon. They are a suggestion only and entirely up to you to follow them word by word or ignore them. It is recommended discussing before the Game about the preferred playstyle of your Faction mates and who wishes to take upon which playstyle.
                </Typography>
                <Typography component="ul" paragraph>
                    <li><strong>Fellow</strong> (recommended: 4-6) - read the Summary, skim the rest, and <strong>follow</strong> your teammates for the rest. Run where asked, carry what's valuable, fight when called, feast when possible and go hunting as a Hound.</li>
                    <li><strong>Foreman</strong> (recommended: 2-4) - read the Rules and skim the appendix. Throne will be a <strong>tactical</strong> game for you - implement strategy to get stuff done. Navigate the forest, lead your teammates, acquire resources, scout for opportunities and talk diplomacy.</li>
                    <li><strong>Leadership</strong> (recommended: 1-2) - study all rules. As in "Real Time Strategy" genre games, you will decide on team <strong>strategy</strong>: balance effort and resources between pillars to maximize Victory Points, coordinate your team, forge alliances and lead wars.</li>
                </Typography>
                <Typography paragraph>
                    Second, there are <strong>3 official roles</strong> per faction. Each role must be assigned to a different person:
                </Typography>
                <Typography component="ul" paragraph>
                    <li><strong>Heir</strong> - represents and makes decisions on behalf of the Faction when talking to Game Masters. If the Faction wins, the Heir will become the current King and roleplay him in the Feast next year.</li>
                    <li><strong>Tech support</strong> - responsible for having and setting up a mobile device for Throne Game App (recommended 2 devices and a power bank).</li>
                    <li><strong>Armorer</strong> - responsible for the equipment (weapons & armor). The Armorer must have a list of all team equipment and owners including equipment lent by game masters. They must ensure the equipment is used safely and not damaged during the game, as well as returned properly to owners after the game ends.</li>
                </Typography>
            </Section>
            <Section heading="Historical Rules">
                <Typography paragraph>
                    Rules of all previous games since 2019.
                </Typography>
                <Grid container spacing={4}>
                    {RULE_GAMES.slice(1).map((ruleGame) => (
                        <Grid item key={ruleGame.title} xs={12} lg={6}>
                            <Typography variant='h3' gutterBottom>
                                {ruleGame.title}
                            </Typography>
                            <RuleCards items={ruleGame.files} placement="history" />
                        </Grid>
                    ))}
                </Grid>
            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
