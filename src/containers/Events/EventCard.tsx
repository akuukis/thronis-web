import * as React from 'react'

import {
    Card,
    CardContent, Theme, Typography,
} from '@material-ui/core'
import { SxProps } from '@material-ui/system/styleFunctionSx/styleFunctionSx'


import { createFC } from 'src/common/createFC'
import IconFacebook from 'src/components/icons/IconFacebook'
import { UniversalLink } from 'src/components/UniversalLink'

import { IEvents } from './common'


interface EventCardProps {
    item: IEvents,
    sx?: SxProps<Theme>,
}

export const EventCard = createFC(import.meta.url)<EventCardProps>(({
    item,
    theme,
    sx = {},
}) => {
    sx = { ...{
        'borderColor': theme.palette.primary.main,
        'background': theme.palette.background.default,
        'height' : '100%',
        'display' : 'flex',
        'justifyContent' : 'center',
        'alignItems' : 'center',
        padding: 1.5,
    }, ...sx }

    return (
        <Card
            variant="outlined"
            square={false}
            sx={sx}
        >
            <CardContent sx={{ padding: '0 !important' }}>
                <Typography align='center' variant='h6'>{item.date}</Typography>
                <Typography align='center'>{item.title}</Typography>
                <Typography align='center' sx={{ paddingTop: 1 }}>
                    {item.link ? (
                        <UniversalLink href={item.link}><IconFacebook color='primary' fontSize='large' /></UniversalLink>
                    ) : (
                        <IconFacebook fontSize='large' sx={{ color: '#000', opacity: 0.2 }} />
                    )}
                </Typography>
            </CardContent>
        </Card>
    )
})
