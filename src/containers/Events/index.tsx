import React from 'react'

import { Box, Card, Grid, Link, Typography } from '@material-ui/core'
import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber'
import RoomIcon from '@material-ui/icons/Room'
import WatchLaterIcon from '@material-ui/icons/WatchLater'


import emoji_swords from 'assets/events/emoji_swords.png'

import { createFC, useBreakpoint } from 'src/common'
import { ROUTE } from 'src/constants'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'


import { EVENTS } from './common'
import { EventCard } from './EventCard'


export default createFC(import.meta.url)(({ theme }) => {
    const isLgUp = useBreakpoint('lg')

    return (
        <Container
            overline="Pasākumi pirms spēles + spēle"
            title="Pasākumu kalendārs"
            description="No jūlija sākuma līdz pašai spēlei augustā ik nedēļu notiek Troņa iesildīšanās pasākumi dažādām gaumēm. Atrodi sev tīkamos un piedalies!"
        >
            <Section heading="Zobencīņu treniņi">
                <Grid container spacing={isLgUp ? 4 : 2}>
                    <Grid item xs={12} lg={6}>
                        <Card sx={{ background: theme.palette.background.paper, padding: theme.spacing(2, 1), marginBottom: 2 }}>
                            <Box sx={{
                                display: 'flex',
                                height: '100%',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}
                            >
                                <Box>
                                    <Typography><RoomIcon fontSize='inherit' sx={{ verticalAlign: 'middle' }} /> Grīziņkalna virsotne</Typography>
                                    <Typography><WatchLaterIcon fontSize='inherit' sx={{ verticalAlign: 'middle' }} /> 18:30 - 21:00 katru otrdienu un ceturtdienu <strong>(sākot ar Jūlija vidu)</strong></Typography>
                                    <Typography><ConfirmationNumberIcon fontSize='inherit' sx={{ verticalAlign: 'middle' }} /> Dalība <strong>bezmaksas</strong></Typography>
                                </Box>
                                <Box component="img" height={60} width='auto' src={emoji_swords} sx={{ marginRight: 2 }} />
                            </Box>
                        </Card>
                        <Typography paragraph>Treneri Elons vai Aksels iemācīs pašus LARP zobencīņu pamatus gan cīņai duelī, gan komandās.</Typography>
                        <Typography paragraph>Treniņi piemēroti gan pilnīgiem iesācējiem, gan LARP meistariem.</Typography>
                        <Typography paragraph>Pamata ekipējumu nodrošinām, bet, ja tev ir pašam savs LARP zobens, loks vai vairogs - ņem līdzi!</Typography>
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={1}>
                            {Object.values(EVENTS.filter(({ type }) => type === 'training').map((item, index) => (
                                <Grid key={index} item xs={12} sm={6} lg={4}>
                                    <EventCard item={item} />
                                </Grid>
                            )))}
                        </Grid>
                    </Grid>
                </Grid>
            </Section>
            {/* <Section heading="Pasēdēšanas">
                <Grid container spacing={isLgUp ? 4 : 2}>
                    <Grid item xs={12} lg={6}>
                        <Card sx={{ background: theme.palette.background.paper, padding: theme.spacing(2, 1), marginBottom: 2 }}>
                            <Box sx={{
                                display: 'flex',
                                height: '100%',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}
                            >
                                <Box>
                                    <Typography><RoomIcon fontSize='inherit' sx={{ verticalAlign: 'middle' }} /> Bufete Gauja</Typography>
                                    <Typography><WatchLaterIcon fontSize='inherit' sx={{ verticalAlign: 'middle' }} /> 19:00 - 21:00 pa kādai trešdienai </Typography>
                                    <Typography><ConfirmationNumberIcon fontSize='inherit' sx={{ verticalAlign: 'middle' }} /> Dalība <strong>bezmaksas</strong></Typography>
                                </Box>
                                <Box component="img" height={60} width='auto' src={emoji_party} sx={{ marginRight: 2 }} />
                            </Box>
                        </Card>
                        <Typography paragraph>Tiekamies gan ar organizatoriem, gan dalībniekiem, un komandu līderiem lai iepazītos un izrunātu jautājumus par spēli.</Typography>
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={1}>
                            {Object.values(EVENTS.filter(({ type }) => type === 'party').map((item, index) => (
                                <Grid key={index} item xs={12} sm={6} lg={6}>
                                    <EventCard item={item} />
                                </Grid>
                            )))}
                        </Grid>
                    </Grid>
                </Grid>
            </Section> */}
            <Section heading="Spēle Tronis XI">
                <Grid container spacing={isLgUp ? 4 : 2}>
                    <Grid item xs={12} lg={6}>
                        <Typography paragraph>Šīs vasaras lielākais notikums - XI 30. - 31. augustā.</Typography>
                        <Typography paragraph>Uzzini kā ierasties <Link href={ROUTE.VENUE}>pareizajā vietā un laikā</Link> uz spēli un zini ko sagaidīt!</Typography>
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={1}>
                            {Object.values(EVENTS.filter(({ type }) => type === 'game').map((item, index) => (
                                <Grid key={index} item xs={12}>
                                    <EventCard item={item} />
                                </Grid>
                            )))}
                        </Grid>
                    </Grid>
                </Grid>
            </Section>

        </Container>
    )
}) /* =============================================================================================================== */
