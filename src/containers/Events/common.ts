export interface IEvents {
    type: 'training' | 'boardgame' | 'party' | 'game'
    date: string,
    title: string,
    link: string | null,
}

export const EVENTS: IEvents[] = [
    // { type: 'training', date: '7. jūlijs'  , title: 'Tronis IX - treniņi 1/10  ', link: 'https://www.facebook.com/events/393803292722643/393803299389309' }, //exemple link for axels


    { type: 'training', date: 'Informācija'  , title: 'Treniņi vasarā', link: 'https://www.facebook.com/LARTS.Throne' },
  
    //{ type: 'party', date: '20. jūlijs'  , title: 'Tronis XI - pasēdēšana 1/2 ', link: 'https://www.facebook.com/events/1240119773410669' },
    //{ type: 'party', date: '10. augusts' , title: 'Tronis XI - pasēdēšana 2/2 ', link: 'https://www.facebook.com/events/1675543006165557' },
    { type: 'game', date: '30.-31. augusts', title: 'Piedzīvojumu spēle "Tronis XI"', link: 'https://www.facebook.com/LARTS.Throne' },
]

