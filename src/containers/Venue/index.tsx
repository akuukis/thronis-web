import React from 'react'

import { Box, Button, Card, Grid, Icon, Typography } from '@material-ui/core'

import battle from 'assets/venue/battle.jpg'
import gm_href from 'assets/venue/gm_href.png'
import map from 'assets/venue/map_2023_bkg.png'
import osm_href from 'assets/venue/osm_href.png'
import programm from 'assets/venue/programm.jpg'
import waze_href from 'assets/venue/waze_href.png'


import { createFC, useBreakpoint } from 'src/common'
import { ContactCard } from 'src/components/ContactCard'
import { EventCard } from 'src/components/EventCard'
import IconFacebook from 'src/components/icons/IconFacebook'
import RuleCards from 'src/components/RuleCards'
import { RULE_GAMES } from 'src/components/RuleCards/common'
import { Table } from 'src/components/Table'
import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

import { CONTACTS } from '../Contacts/common'

import { EVENTS } from './common'


export default createFC(import.meta.url)(({ theme }) => {
    const lgUp = useBreakpoint('lg')

    return (
        <Container
            overline="Spēle 2025"
            title="Tronis XI"
            description="Zini, kas notiks un ierodies pareizajā vietā!"
        >
            <Section heading="Noteikumi">
                <Grid container spacing={3}>
                    <Grid item xs={12} lg={6}>
                        <Typography>
                            <strong>Visiem dalībniekiem jāizlasa noteikumu apkopojums (summary) </strong>.
                            Ne visiem dalībniekiem jāizlasa pilnie noteikumi, bet vismaz vienam komandas dalībniekam tie labi jāpārzin.
                            Noteikumi angļu valodā.
                        </Typography>
                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <RuleCards items={RULE_GAMES[0].files} placement='highlight' />
                    </Grid>
                </Grid>
            </Section>
            <Section heading="Laiks un vieta">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6} sx={{ overflow: 'hidden' }}>
                        <Box
                            component="img"
                            src={map}
                            alt='karte'
                            sx={{ minHeight: 300, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'center' }}
                        />
                    </Grid>
                    <Grid item xs={12} lg={6} alignItems="center" sx={{ display: 'flex', alignItems: 'center' }}>
                        <Card sx={{ background: theme.palette.background.default, [theme.breakpoints.up('sm')]: { padding: theme.spacing(4, 5) }  }}>
                            <Typography align="center" variant="h3" sx={{ marginBottom: 2 }}>30.-31. augusts</Typography>
                            <Table
                                sx={{ marginBottom: 4 }}
                                // columns={['Dzīres', 'Spēle', 'Afterītis']}
                                // rows={[['piektdiena', 'sestdiena', 'sestdiena +'], ['19.00', '10.00', '19.00']]}
                                // 
                                columns={['Programma', 'Laiks']}
                                rows={[['Dzīres', 'piektdiena, 19.00 - 01.00'], ['Teritorijas apgaita un pils izvēle', 'sestdiena, 8.00-9.30'], ['Spēle', 'sestdiena, 10.00-17.00'], ['Lielā kauja', 'sestdiena, 18.00-18.30']]}
                            />
                            <Typography align="center" variant="h3" sx={{ marginBottom: 2 }}>
                                Suntaži, Ogres novads
                            </Typography>
                            <Typography align="center" paragraph>
                                Tronis notiek 6ha spēles poligonā Aksela mežā. Uz poligonu nevar nokļūt nekā savādāk kā ar orgu organizētu transportu.
                            </Typography>
                            <Typography align="center">
                                <UniversalLink href="https://www.facebook.com/events/788315866242741" title="facebook" blank>
                                    <Typography color='inherit' noWrap><IconFacebook sx={{ verticalAlign: 'middle' }} />{' '} Tronis XI Facebook pasākums</Typography>
                                </UniversalLink>
                            </Typography>
                        </Card>

                    </Grid>
                </Grid>
            </Section>
            <Section heading="Transports">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}>
                        <Card sx={{
                            padding: theme.spacing(5.25),
                            background: theme.palette.background.paper,
                            height: '100%',
                        }}
                        >
                            <Typography variant="body2" sx={{ marginBottom: 2, fontWeight: 'bold' }}>
                                Uz spēli var nokļūt ar:
                            </Typography>
                            <Typography>🚗&nbsp;&nbsp;auto</Typography>
                            <Typography>
                                🚈&nbsp;&nbsp;
                                <UniversalLink href="https://www.pv.lv/lv/marsrutu-saraksts/?from=R%C4%ABga&stop_from=1&to=Ogre&stop_to=339&date=11.08.2023&date-h=10.08.2023" title="vilcienu" blank>
                                    vilcienu{' '}
                                </UniversalLink>
                                līdz Ogres stacijai*
                            </Typography>
                            <Typography>
                                🚌&nbsp;&nbsp;
                                <UniversalLink href="https://www.1188.lv/satiksme/saraksti/riga/suntazi/200001/104818/diena/2023-08-11" title="autobusu" blank>
                                    autobusu
                                </UniversalLink>
                                {' '}līdz pieturai Suntaži*
                            </Typography>
                            <Typography sx={{ marginTop: 2, marginBottom: 2 }}>
                                * iepriekš vienojoties, organizatori savāks no pieturas un aizvedīs līdz poligonam
                            </Typography>
                            <Typography>
                                <UniversalLink href="https://www.openstreetmap.org/?mlat=56.93074&mlon=24.94480#map=17/56.93074/24.94480&layers=N" title="OpenStreeMap" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <img src={osm_href} alt='OpenStreetMap' />
                                    </Icon>
                                    {' '}OpenStreetMap
                                </UniversalLink>
                            </Typography>
                            <Typography>
                                <UniversalLink href="https://ul.waze.com/ul?ll=56.93074454%2C24.94480275&navigate=yes" title="Waze" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <img src={waze_href} alt='Waze' />
                                    </Icon>
                                    {' '}Waze
                                </UniversalLink>
                            </Typography>
                            <Typography>
                                <UniversalLink href="https://goo.gl/maps/dU4d9tkgv8igs2Fn9" title="Google Maps" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <img src={gm_href} alt='Google Maps' />
                                    </Icon>
                                    {' '}Google Maps
                                </UniversalLink>
                            </Typography>
                        </Card>

                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <Box
                            component='iframe'
                            width="425"
                            height="300"
                            src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d742456.1388458747!2d24.193960913376383!3d56.96311515455828!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e0!4m5!1s0x46eecfb0e5073ded%3A0x400cfcd68f2fe30!2zUsSrZ2E!3m2!1d56.967694099999996!2d24.105622099999998!4m3!3m2!1d56.9341667!2d24.9398889!5e1!3m2!1sen!2slv!4v1691620529872!5m2!1sen!2slv"
                            sx={{ minHeight: 300, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'top' }}
                        />
                    </Grid>
                </Grid>
            </Section>
            <Section heading="Programma">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}><EventCard event={EVENTS[0]} /></Grid>
                            <Grid item xs={12}><EventCard event={EVENTS[1]} /></Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={6} sx={{ overflow: 'hidden' }} hidden={!lgUp}>
                        <Box
                            component="img"
                            src={programm}
                            alt='Spēle un Dzīres'
                            sx={{ minHeight: 360, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'top' }}
                        />
                    </Grid>
                </Grid>
                <Grid container direction='row-reverse' spacing={2} sx={{ marginTop: lgUp ? 2 : 0 }}>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}><EventCard event={EVENTS[2]} /></Grid>
                            <Grid item xs={12}><EventCard event={EVENTS[3]} /></Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={6} sx={{ overflow: 'hidden' }}>
                        <Box
                            component="img"
                            src={battle}
                            alt='Lielā kauja un Afterītis'
                            sx={{ minHeight: 360, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'center' }}
                        />
                    </Grid>
                </Grid>
            </Section>
            <Section heading="Kontakti">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}><ContactCard contact={CONTACTS[0]} /></Grid>
                    <Grid item xs={12} lg={6} sx={{ display: 'flex' }}>
                        <Grid container alignItems="center">
                            <Grid item sx={{ marginLeft: 5.25, marginRight: 5.25 }}>
                                <Typography align="center">
                                    Spēles galvenais organzators ir Kristaps Krūtainis (Aksels). Ar viņu vari sazināties jebkurā ar
                                    spēli saistītā sakarā.
                                </Typography>
                                <Typography align="center" sx={{ marginTop: 2 }}>
                                    <Button href={ROUTE.CONTACTS} variant="outlined" sx={{ margin: 1 }}>
                                        Visi organizatori
                                    </Button>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
