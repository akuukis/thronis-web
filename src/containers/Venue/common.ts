import { IEvents } from 'src/components/EventCard'


export const EVENTS: IEvents[] = [
    {
        heading: 'Dzīres',
        date: 'Piektdiena, 30. augusts,',
        time: '20.00 - 01.00',
        paragraph: 'Lomu spēļu vakars, kurā komandas cīnās par labākajām pozīcijām spēles sākumam. Vakara gaitā dalībnieki iepazīst spēles mehānikas, citus dalībniekus un iegūst resursus spēles sākumam.',
    },
    {
        heading: 'Spēle',
        date: 'Sestdiena, 31. augusts,',
        time: '10.00 - 17.00',
        paragraph: 'Spēle ar 5 sezonām, kurā komandas sacenšas par labākās titulu iekaustot pretiniekus, vācot resursus un veidojot diplomātiskas attiecības ar konkurentiem.',
    },
    {
        heading: 'Lielā kauja',
        date: 'Sestdiena, 31. augusts,',
        time: '18.00 - 18.30',
        paragraph: 'Noslēguma lielā kauja, kurā visas komandas vienuviet cīnās par pēdējiem uzvaras punktiem.',
    },
    {
        heading: 'Rezultāti un afterītis',
        date: 'Sestdiena, 31. augusts,',
        time: '19.00 - nākamās dienas 12.00',
        paragraph: 'Foto, apbalvošana un pēcspēles pasēdēšana ar ugunskuru un desiņām turpat lielās kaujas norises vietā. Stāsti par katra spēles piedzīvojumiem.',
    },

]
