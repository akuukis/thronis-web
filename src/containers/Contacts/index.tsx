import React from 'react'

import { Grid, useMediaQuery } from '@material-ui/core'

import { createFC } from 'src/common'
import { ContactCard } from 'src/components/ContactCard'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

import { CONTACTS } from './common'


export default createFC(import.meta.url)(({ children, theme }) => {
    const isLg = useMediaQuery(theme.breakpoints.up('lg'))

    return (
        <Container
            overline="Komandas sastāvs"
            title="Kontakti"
            description="Lai cik smagi jautājumi vai lieliski piedāvājumu tev būtu uz sirds, droši sazinies ar orgiem!"
        >
            <Section id='kontakti'>
                <Grid container spacing={isLg ? 4 : 1}>
                    {Object.values(CONTACTS).map((value) => <Grid item xs={12} lg={6}><ContactCard contact={value} /></Grid>)}
                </Grid>
            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
