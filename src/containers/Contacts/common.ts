import contactAndrievs from 'assets/contacts/contactAndrievs.png'
import contactAnnija from 'assets/contacts/contactAnnija.png'
import contactAxels from 'assets/contacts/contactAxels.png'
import contactElon from 'assets/contacts/contactElon.png'


export interface IContact {
    image: string,
    position: string,
    fullName: string,
    phone: string | null,
    email: string | null,
    discord: string | null,
    facebook: string | null,
}

export const CONTACTS: IContact[] = [
    // {
    //     position: 'Galvenais organizators',
    //     fullName: 'Kalvis Kalniņš',
    //     phone: '+371 22118827',
    //     email: 'kalvis@kalvis.lv',
    //     discord: 'Akuukis#1654',
    //     facebook: 'kalvis.kalnins.3',
    //     image: contactKalvis,
    // },
    {
        position: 'Galvenais organizators',
        fullName: 'Kristaps Krūtainis (Axels)',
        phone: '+371 26471183',
        email: 'kkrutainis@gmail.com',
        discord: 'AxelBleidd#7755',
        facebook: 'kristaps.krutainis',
        image: contactAxels,
    },
    {
        position: 'Galvenais organizators',
        fullName: 'Artyom Elon Tatsu',
        phone: '+371 27175063',
        email: null,
        discord: 'Elon#6151',
        facebook: null,
        image: contactElon,
    },
    // {
    //     position: 'Galvenā organizatore',
    //     fullName: 'Rota Kalniņa',
    //     phone: '+371 22036914',
    //     email: 'rota@bruna.lv',
    //     discord: 'RuBru#6619',
    //     facebook: 'rota.bruuna',
    //     image: contactRota,
    // },
    // {
    //     position: '#Loģistika',
    //     fullName: 'Aleksis Ozoliņš',
    //     phone: '+371 29211471',
    //     email: null,
    //     discord: 'Ozzy#9443',
    //     facebook: null,
    //     image: contactAleksis,
    // },
    {
        position: '#Kastītes',
        fullName: 'Andrievs Auseklis Auziņš',
        phone: '+371 24602727',
        email: null,
        discord: 'Andrievs#4963',
        facebook: null,
        image: contactAndrievs,
    },
    {
        position: '#PR #Video #Foto',
        fullName: 'Annija Lūcija Frīdmane',
        phone: null,
        email: null,
        discord: 'kaulinsh#3140',
        facebook: null,
        image: contactAnnija,
    },

]
