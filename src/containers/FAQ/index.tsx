import React, { useCallback } from 'react'

import { createFC } from 'src/common'
import { FAQ_ENTRIES } from 'src/common/faq'
import AccordionFaq from 'src/components/AccordionFaq'
import Container from 'src/layouts/DefaultLayout/Container'


export default createFC(import.meta.url)(({ children, theme }) => {
    const [expanded, setExpanded] = React.useState<string | false>(false)

    const handleChange = useCallback((panel: string) => () => {
        setExpanded((expandedPrev) => expandedPrev !== panel ? panel : false)
    }, [])

    return (
        <Container
            overline="Biežāk uzdotie jautājumi"
            title="FAQ"
            description="Konkrētas atbildes uz konkrētiem jautājumiem."
        >
            {FAQ_ENTRIES.map((faq) => (
                <AccordionFaq
                    key={faq.id}
                    faq={faq}
                    expanded={expanded === faq.id}
                    onChange={handleChange(faq.id)}
                />
            ))}
        </Container>
    )
})
