import * as React from 'react'

import { Card, CardContent, Grid, Typography } from '@material-ui/core'
import IconShare from '@material-ui/icons/ShareOutlined'

import { createFC } from 'src/common/createFC'
import IconFacebook from 'src/components/icons/IconFacebook'
import Section from 'src/layouts/DefaultLayout/Section'

import { HistoryItem } from './common'
import HIstoryItemRow from './HIstoryItemRow'


export interface IEvents {
    heading: string,
    date?: string,
    time: string,
    paragraph: string,
}
interface Props {
    event: HistoryItem;
}

export default createFC(import.meta.url)<Props>(({ theme, event }) => {
    return (
        <Card
            variant="elevation"
            sx={{
                [theme.breakpoints.down('md')]: {
                    margin: theme.spacing(0, -2),
                },
            }}
        >
            <CardContent
                sx={{
                    padding: theme.spacing(2,2,0,2),
                    [theme.breakpoints.up('md')]: {
                        padding: theme.spacing(4, 4, 2, 4),
                    },
                }}
            >
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}>
                        <Section id={event.title} heading={event.title}>
                            <Typography paragraph>
                                {event.text}
                            </Typography>
                            {event.games.map((game) => (
                                <React.Fragment key={game.title}>
                                    <Typography variant='overline'>{game.overline}</Typography>
                                    <Typography variant='h3'>{game.title}</Typography>
                                    <Typography paragraph>{game.text}</Typography>
                                </React.Fragment>
                            ))}
                            {event.facebook && <HIstoryItemRow icon={<IconFacebook />} {...event.facebook} /> }
                            {event.website && <HIstoryItemRow icon={<IconShare />} {...event.website} /> }
                        </Section>
                    </Grid>
                    <Grid component="figure" item xs={12} lg={6}>
                        <img
                            alt={event.image.title}
                            src={event.image.url}
                            width='100%'
                        />
                        <Typography component='figcaption' variant='caption' align='center'>{event.image.title}</Typography>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    )
})
