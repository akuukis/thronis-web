import React from 'react'

import { Grid } from '@material-ui/core'

import { createFC } from 'src/common'
import Container from 'src/layouts/DefaultLayout/Container'

import { HISTORY_ITEMS } from './common'
import HIstoryItem from './HIstoryItem'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    return (
        <Container
            overline="LARP Latvijā"
            title="Organizatoru komandas"
            description="Latvijā ik gadu notiek vairāk kā 3 spēles no dažādām organizatoru komandām. Lūk, spēcīgākās LARP organizatoru komandas un to zināmākās spēles, kas parasti notiek reizi gadā."
        >
            <Grid container spacing={4}>
                {HISTORY_ITEMS.map((historyItem) => (
                    <Grid key={historyItem.title} item>
                        <HIstoryItem event={historyItem} />
                    </Grid>
                ))}
            </Grid>
        </Container>
    )
}) /* =============================================================================================================== */
