import * as React from 'react'

import { Grid, Typography } from '@material-ui/core'

import { createFC } from 'src/common/createFC'
import { UniversalLink } from 'src/components/UniversalLink'

import { HistorySubitem } from './common'


export interface IEvents {
    heading: string,
    date?: string,
    time: string,
    paragraph: string,
}
interface Props extends HistorySubitem {
    icon: JSX.Element
}

export default createFC(import.meta.url)<Props>(({ theme, icon, url, title }) => {
    return (
        <Grid container spacing={1}>
            <Grid item>{icon}</Grid>
            <Grid item>
                <Typography>
                    <UniversalLink href={url}>{title}</UniversalLink>
                </Typography>
            </Grid>
        </Grid>
    )
})
