import kalvis_co from 'assets/latvia/kalvis_co.jpg'
import suntazi from 'assets/latvia/suntazi.png'
import sword_of_baltic from 'assets/latvia/sword_of_baltic.png'


export interface HistorySubitem {
    title: string
    url: string
}

export interface HistoryGame {
    overline: string
    title: string
    text: string
}

export interface HistoryItem {
    title: string
    text: string
    image: HistorySubitem
    website?: HistorySubitem
    facebook?: HistorySubitem
    games: HistoryGame[]
}

export const HISTORY_ITEMS: HistoryItem[] = [
    {
        title: 'Suntaži',
        text: 'Aksela mājas ir mājas latviešu LARP kopienai. Tur ir gan pasaku mežs ar krogu, gan noliktava mantām. Suntažu komanda rīko vairākus pasākumus, no tiem klasiskākais ir Krēslas Hronikas.',
        image: { title: 'spēlētāji pie galda krodziņā Krēslas hroniku spēles laikā Suntažos', url: suntazi },
        website: { title: 'Krēslas hroniku mājaslapa', url: 'https://www.inner-darkness.com/' },
        facebook: { title: 'Krēslas hroniku facebook grupa', url: 'https://www.facebook.com/groups/kreslas.hronikas' },
        games: [
            {
                overline: 'Oktobris',
                title: 'Krēslas hronikas',
                text: 'Dark Fantasy LARP pārsvarā fokusēts uz pēcnāves un tumsas valstībām. Viens no visilgāk turpinātājiem LARP projektiem Latvijā.',
            },
        ],
    },
    {
        title: 'Sword of the Baltic',
        text: 'Amigas jeb Nikolaja Petrova komanda jau izsenis rīko LARP pasākumus, kuros pamatvaloda ir krievu. Šajos LARP svarīgi ierasties labi un skaisti bruņotiem.',
        image: { title: 'attēls no Sword of Baltic spēles', url: sword_of_baltic },
        website: { title: 'Sword of the Baltic mājaslapa', url: 'https://balticsword.eu' },
        games: [
            {
                overline: 'Augusts',
                title: 'Witcher',
                text: 'Populārās datorspēles Witcher pasaulē balstīta LARP spēle komandās ar sacensību elementiem.',
            },
        ],
    },
    {
        title: 'Kalvis & Co',
        text: 'Kalvis Kalniņš ar plašu organizatoru komandu 5 gadu garumā veidoja Troni un ir padarījuši to par gan jauniņajiem, gan visu tautību LARP spēlētājiem draudzīgu pasākumu.',
        image: { title: 'Troņa orgu komanda mazajā Tronī, kamēr Elons vēl strādā', url: kalvis_co },
        website: { title: 'Troņa mājaslapa', url: 'https://tronis.lv' },
        facebook: { title: 'Troņa facebook lapa', url: 'https://www.facebook.com/LARTS.Throne' },
        games: [
            {
                overline: 'Augusts',
                title: 'Tronis',
                text: 'Spēle vieglā viduslaiku fantāzijā ar uzsvaru uz stratēģiju, cīnīšanos, komandas sadarbību un trauslām aliansēm.',
            },
        ],
    },
]
