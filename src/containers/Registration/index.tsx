import React from 'react'

import { Card, CardContent, Grid, Typography } from '@material-ui/core'

import { createFC, useBreakpoint } from 'src/common'
import { ContactCard } from 'src/components/ContactCard'
import { UniversalLink } from 'src/components/UniversalLink'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

import { CONTACTS } from '../Contacts/common'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(({ theme }) => {
    const mdUp = useBreakpoint('md')

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const contactRota = CONTACTS.find((contact) => contact.fullName === 'Kristaps Krūtainis (Axels)')!

    return (
        <Container
            overline="Reģistrējies spēlei"
            title="Pieteikšanās"
            description="Lai kļūtu par dalībnieku, sazinies ar Kristapu Krūtainis (Axelu) un izpildi 4 punktus sarakstā."
        >
            <Section
                overline='Reģistrēšanās'
                heading='Kā kļūt par dalībnieku?'
            >
                <Grid container spacing={mdUp ? 4 : 2}>
                    <Grid item xs={12} md={6}>
                        <Typography component='div' paragraph>
                            👉 Uzraksti Kristapam, ka vēlies piedalīties un info par sevi:
                            <Typography component='ul'>
                                <li>savu pilno vārdu uzvārdu</li>
                                <li>telefona numuru</li>
                            </Typography>
                        </Typography>
                        <Typography paragraph>
                            ✅ Uzliec "going" pasākuma <UniversalLink href='https://www.facebook.com/events/788315866242741'>Facebook pasākumā</UniversalLink> (ja izmanto FB)
                        </Typography>
                        <Typography paragraph>
                            📣 Pievienojies Troņa <UniversalLink href='https://discord.gg/D8ZSqjee4B'> Discord serverim</UniversalLink> (ja izmanto Discord)
                        </Typography>
                        <Typography paragraph>
                            💰 Piemeties kopīgajām izmaksām 25 eur pēc Tavas izvēles šeit:
                        </Typography>
                        <Card>
                            <CardContent>
                                <Typography variant='body2'>
                                    LV41UNLA0050006642673 <br />
                                    SEB<br />
                                    Kristaps Krūtainis<br />
                                    Reference: Mana daļa Tronis XI kopīgo izmaksu segšanai no [vārds uzvārds]
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <ContactCard contact={contactRota} />
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h3'>
                            Tronim var pieteikties ikviens gribētājs, arī tu!
                        </Typography>
                    </Grid>
                </Grid>

            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
