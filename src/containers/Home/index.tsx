import React, { useState } from 'react'

import { Box, Chip, Grid, alpha } from '@material-ui/core'

import banner3 from 'assets/banner-home-3.png'
import banner6 from 'assets/banner-home-6.png'
import banner7 from 'assets/banner-home-7.png'
import banner8 from 'assets/banner-home-8.png'

import { createFC, useBreakpoint } from 'src/common'
import SectionFaq from 'src/components/SectionFaq'
import SectionStuff from 'src/components/SectionStuff'
import SectionThisYear from 'src/components/SectionThisYear'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'


interface IProps {
}

const IMAGES = [
    banner3,
    banner6,
    banner7,
    banner8,
]

const KEYPHRASES = [
    'komandu sports viduslaiku noskaņās 🤺🤺',
    'RTS LARP 🎲',
    'kā airsoft, bet ar drošiem zobeniem ⚔',
    'D&D mežā 🌳',
    'aktīvā atpūta dabā 🏕',
]

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const mdUp = useBreakpoint('md')
    const [images] = useState(IMAGES.slice().sort(() => Math.random() - 0.5))
    const [luckyKeyphrase] = useState(Math.floor(Math.random() * KEYPHRASES.length))

    return (
        <Container
            banner={(
                <Grid container wrap='nowrap'>
                    <Grid item xs><Box component='img' src={images[0]} sx={{ display: 'block', objectFit: 'cover', objectPosition: 'top', width: '100%', height: 420 }} /></Grid>
                    <Grid item xs><Box component='img' src={images[1]} sx={{ display: 'block', objectFit: 'cover', objectPosition: 'top', width: '100%', height: 420 }} /></Grid>
                    <Grid item xs><Box component='img' src={images[2]} sx={{ display: 'block', objectFit: 'cover', objectPosition: 'top',width: '100%', height: 420 }} /></Grid>
                </Grid>
            )}
            overline="Spēle"
            title="Tronis"
            description="Vienas dienas stratēģijas lomu spēle brīvā dabā ar drošajiem zobeniem."
        >
            <Box sx={{ position: 'relative' }}>
                <Box sx={{
                    display: 'none',
                    [theme.breakpoints.up('md')]: {
                        justifyContent: 'center',
                        flexWrap: 'wrap',
                        display: 'flex',
                        position: 'absolute',
                        bottom: theme.spacing(12+2),
                        zIndex: 1,
                        margin: theme.spacing(0, -8),
                        width: `calc(100% + ${theme.spacing(16)})`,
                    },
                }}
                >
                    {KEYPHRASES.map((text, i) => (
                        <Chip
                            key={text}
                            label={text}
                            sx={{
                                margin: theme.spacing(1),
                                backgroundColor: alpha(i === luckyKeyphrase ? theme.palette.secondary.main : '#000', 0.75),
                                color: theme.palette.primary.contrastText,
                                ...theme.typography.body2,
                            }}
                        />
                    ))}
                </Box>
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    flexWrap: 'wrap',
                    width: '100%',
                    backgroundColor: alpha('#000', 0.6),
                    // padding: theme.spacing(2, 2),
                    [theme.breakpoints.up('md')]: {
                        display: 'none',
                    },
                }}
                >
                    <Grid container wrap='nowrap'>
                        <Grid item xs>
                            <Box component='img' src={images[0]} sx={{ display: 'block', objectFit: 'cover', width: '100%', height: 420 }} />
                        </Grid>
                    </Grid>
                    <Section heading='Kas ir Tronis?' whiteHeading>
                        <Grid container spacing={1}>
                            {KEYPHRASES.map((text, i) => (
                                <Grid item key={text}>
                                    <Chip
                                        label={text}
                                        sx={{
                                            ...theme.typography.body2,
                                            color: theme.palette.primary.contrastText,
                                            backgroundColor: alpha('#000', i === luckyKeyphrase ? 0.75 : 0.2),
                                        }}
                                    />
                                </Grid>
                            ))}
                        </Grid>
                    </Section>
                    <Section id='mobile-video' sx={{ width: '100%' }}>
                        <iframe
                            width="100%"
                            height="420"
                            src="https://www.youtube-nocookie.com/embed/QX-zDErnVGQ"
                            title="YouTube video player"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                        />
                    </Section>
                </Box>
            </Box>
            {mdUp && (
                <Section
                    id='video'
                    overline="Ieskata Video"
                    heading="Kas ir Tronis?"
                >
                    <iframe
                        width="100%"
                        height="500"
                        src="https://www.youtube-nocookie.com/embed/QX-zDErnVGQ"
                        title="YouTube video player"
                        frameBorder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                    />
                </Section>
            )}
            <SectionThisYear />
            <SectionStuff />
            {/* <SectionEvents /> */}
            {/* <SectionSponsors /> */}
            <SectionFaq />
        </Container>
    )
}) /* =============================================================================================================== */
