import { useEffect } from 'react'

import { useLocation } from 'react-router-dom'

import { createFC } from 'src/common'


export default createFC(import.meta.url)(({ children, theme }) => {
    const location = useLocation()

    useEffect(() => {
        (window as any).goatcounter?.count({
            path: location.pathname,
        })
    }, [location.pathname])

    return null
}) /* =============================================================================================================== */
