import React, { useCallback, useState } from 'react'

import { AppBar, BottomNavigation, BottomNavigationActionProps, Drawer, IconButton, List, Paper, alpha } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'
import IconMenu from '@material-ui/icons/Menu'
import { Box } from '@material-ui/system'

import { createFC, useBreakpoint } from 'src/common'
import { ROUTE } from 'src/constants'

import MyMenuListItem from './MyMenuListItem'


interface IProps extends BottomNavigationActionProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme, ...props }) => {
    const [open, setOpen] = useState(false)

    const smUp = useBreakpoint('sm')

    const handleOpen = useCallback(() => setOpen(true), [setOpen])
    const handleClose = useCallback(() => setOpen(false), [setOpen])

    return (
        <>
            <IconButton
                onClick={handleOpen}
                sx={{ color: theme.palette.primary.contrastText, width: smUp ? 72 : 56 }}
                size="small"
            >
                <IconMenu color='inherit' />
            </IconButton>
            <Drawer open={open} anchor={smUp ? 'right' : 'bottom'} onClose={handleClose} keepMounted>
                <Paper sx={{ position: 'relative', backgroundColor: alpha(theme.palette.primary.dark, 0.95), height: smUp ? '100%' : 'initial' }}>
                    {smUp && (
                        <AppBar
                            position='static'
                            sx={{
                                backgroundColor: theme.palette.primary.dark,
                                boxSizing: 'content-box',
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                                height: 72,
                            }}
                        >
                            <IconButton
                                onClick={handleClose}
                                sx={{ color: theme.palette.primary.contrastText }}
                            >
                                <IconClose />
                            </IconButton>
                        </AppBar>
                    )}
                    <List sx={{ color: theme.palette.primary.contrastText, minWidth: 300 }}>
                        <MyMenuListItem onClick={handleClose} href={ROUTE.HOME        } title="Sākums" />
                        <MyMenuListItem onClick={handleClose} href={ROUTE.CURRENT   } title="Tronis 2024" />
                        <MyMenuListItem onClick={handleClose} href={ROUTE.EVENTS      } title="Pasākumu Kalendārs" />
                        <MyMenuListItem onClick={handleClose} href={ROUTE.ITEMS       } title="Mantas" />
                        <MyMenuListItem onClick={handleClose} href={ROUTE.RULES       } title="Noteikumi" />
                        {/* <MyMenuListItem onClick={handleClose} href={ROUTE.HISTORY      } title="Troņa vēsture" /> */}
                        {/* <MyMenuListItem onClick={handleClose} href={ROUTE.LARP        } title="Citi LARP" /> */}
                        <MyMenuListItem onClick={handleClose} href={ROUTE.CONTACTS    } title="Kontakti" />
                        <MyMenuListItem onClick={handleClose} href={ROUTE.REGISTRATION} title="Pieteikties" />
                        {/* <ListItem>
                            <ListItemText primary="&nbsp;" />
                            <ListItemSecondaryAction>
                                <Button color="inherit" startIcon={<Share />}>
                                    Dalīties
                                </Button>
                            </ListItemSecondaryAction></ListItem> */}
                    </List>
                    <Box id="google_translate_element" sx={{ padding: theme.spacing(1, 2), backgroundColor: theme.palette.primary.background }} />
                    {!smUp && (
                        <AppBar position='static' sx={{ backgroundColor: theme.palette.primary.dark }}>
                            <BottomNavigation>
                                <IconButton
                                    onClick={handleClose}
                                    sx={{ color: theme.palette.primary.contrastText }}
                                >
                                    <IconClose />
                                </IconButton>
                            </BottomNavigation>
                        </AppBar>
                    )}
                </Paper>
            </Drawer>
        </>
    )
}) /* =============================================================================================================== */
