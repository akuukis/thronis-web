import React from 'react'

import { BottomNavigationActionProps, Button } from '@material-ui/core'
import { useNavigate } from 'react-router'

import { createFC, useBreakpoint } from 'src/common'
import { foreground } from 'src/common/colors'
import { ROUTE } from 'src/constants'

import IconLogoSmall from './IconLogoSmall'
import logo from './Logo.svg'


interface IProps extends BottomNavigationActionProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme, ...props }) => {
    const smUp = useBreakpoint('sm')
    const mdUp = useBreakpoint('md')

    const navigate = useNavigate()

    return (
        <>
            <Button
                href={ROUTE.HOME}
                onClick={(e) => {e.preventDefault(); navigate(ROUTE.HOME)}}
                sx={{
                    color: theme.palette.primary.contrastText,
                    width: mdUp ? 300 : smUp ? 72 : 56,
                    flexShrink: 0,
                    height: '100%',
                    transition: theme.transitions.create('width'),
                    backgroundColor: mdUp ? theme.palette.primary.dark : undefined,
                    '&:hover': {
                        backgroundColor: mdUp ? foreground(theme.palette.mode, theme.palette.primary.main) : theme.palette.primary.dark,
                    },
                }}
            >
                {mdUp ? <img src={logo} width="166" height="44" /> : <IconLogoSmall color='inherit' fontSize="large" />}
            </Button>
        </>
    )
}) /* =============================================================================================================== */
