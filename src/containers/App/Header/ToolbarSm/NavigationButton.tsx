import React from 'react'

import { Typography } from '@material-ui/core'
import { NavLink } from 'react-router-dom'

import { IMyTheme, createFC, createStyles } from 'src/common'
import { ROUTE } from 'src/constants'


const styles = (theme: IMyTheme) => createStyles({
    root: {
        color: theme.palette.primary.contrastText,
        borderColor: theme.palette.primary.contrastText,
        padding: theme.spacing(2, 1),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(2, 4),
        },
        height: '100%',
        boxSizing: 'border-box',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textDecoration: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
        },
    },
    active: {
        color: theme.palette.primary.background,
        borderBottom: `${theme.spacing(0.5)} solid`,
        paddingTop: theme.spacing(2.5),
    },
    title: {
        overflow: 'hidden',
    },
})


interface IProps {
    to: ROUTE
    title: string
}


export default createFC(import.meta.url, styles)<IProps>(({ children, classes, theme, ...props }) => {
    const { to, title } = props

    return (
        <NavLink
            to={to}
            className={classes.root}
            activeClassName={classes.active}
        >
            <Typography
                noWrap
                className={classes.title}
                variant='button'
            >
                {title}
            </Typography>
        </NavLink>
    )
}) /* ============================================================================================================= */
