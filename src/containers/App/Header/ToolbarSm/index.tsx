import React from 'react'

import { Box, Button, Divider, Toolbar } from '@material-ui/core'
import { useNavigate } from 'react-router'

import { createFC } from 'src/common'
import { ROUTE } from 'src/constants'

import MyMenu from '../common/MyMenu'

import LogoButton from './LogoButton'
import NavigationButton from './NavigationButton'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const navigate = useNavigate()
    return (
        <Toolbar
            disableGutters
            sx={{ maxWidth: theme.breakpoints.values.xl }}
        >
            <LogoButton />
            <Divider flexItem orientation="vertical" />
            <Box sx={{ flexGrow: 1 }} />
            <NavigationButton to={ROUTE.CURRENT} title='Tronis 2024' />
            <NavigationButton to={ROUTE.HISTORY} title='Troņa vēsture' />
            <NavigationButton to={ROUTE.LARP} title='LARP Latvijā' />
            <Box sx={{ flexGrow: 1 }} />
            <Button variant='contained' sx={{ margin: theme.spacing(0, 1) }} onClick={() => navigate(ROUTE.REGISTRATION)}>Pieteikties</Button>
            <Divider flexItem orientation="vertical" />
            <MyMenu />
        </Toolbar>
    )
}) /* =============================================================================================================== */
