import React from 'react'

import { AppBar, Divider } from '@material-ui/core'

import { createFC, useBreakpoint } from 'src/common'
import useGoogleTranslateBarVisibility from 'src/common/useGoogleTranslateBarVisibility'

import ToolbarSm from './ToolbarSm'
import ToolbarXs from './ToolbarXs'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const googleTranslateBarVisibility = useGoogleTranslateBarVisibility()
    const smUp = useBreakpoint('sm')

    return (
        <AppBar sx={{
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                top: googleTranslateBarVisibility ? 38 : 0,
                bottom: 'auto',
            },
            [theme.breakpoints.down('sm')]: {
                top: 'auto',
                bottom: 0,
            },
        }}
        >
            <Divider flexItem orientation="vertical" sx={{ display: { xs: 'none', xl: 'block' } }} />
            {smUp ? <ToolbarSm /> : <ToolbarXs />}
            <Divider flexItem orientation="vertical" sx={{ display: { xs: 'none', xl: 'block' } }} />
        </AppBar>
    )
}) /* =============================================================================================================== */
