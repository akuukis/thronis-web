import React, { useCallback } from 'react'

import { BottomNavigation, Divider } from '@material-ui/core'
import { useNavigate } from 'react-router'

import { createFC } from 'src/common'
import { ROUTE } from 'src/constants'

import MyMenu from '../common/MyMenu'

import MyBottomNavigationAction from './MyBottomNavigationAction'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const navigate = useNavigate()

    const handleChange = useCallback((event, newValue: ROUTE) => {
        navigate(newValue)
    }, [])

    return (
        <BottomNavigation
            sx={{ width: '100%' }}
            showLabels
            onChange={handleChange}
        >
            <MyMenu />
            <Divider flexItem orientation="vertical" />
            <MyBottomNavigationAction value={ROUTE.CURRENT} overline='Tronis' label='2024' />
            <Divider flexItem orientation="vertical" />
            <MyBottomNavigationAction value={ROUTE.HISTORY} overline='Troņa' label='vēsture' />
            <Divider flexItem orientation="vertical" />
            <MyBottomNavigationAction value={ROUTE.LARP} overline='LARP' label='Latvijā' />
        </BottomNavigation>
    )
}) /* =============================================================================================================== */
