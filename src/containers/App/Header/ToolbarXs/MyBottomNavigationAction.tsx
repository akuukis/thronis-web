import React from 'react'

import { BottomNavigationAction, BottomNavigationActionProps, Typography } from '@material-ui/core'

import { createFC } from 'src/common'
import { ROUTE } from 'src/constants'


interface IProps extends BottomNavigationActionProps {
    overline: string
    label: string
}


export default createFC(import.meta.url)<IProps>(({ children, theme, overline, label, ...props }) => {
    return (
        <BottomNavigationAction
            sx={{ color: theme.palette.primary.contrastText, maxWidth: 'unset' }}
            value={ROUTE.CURRENT}
            label={(
                <>
                    <Typography variant='caption'>{overline}</Typography>
                    <Typography variant='h6'>{label}</Typography>
                </>
            )}
            {...props}
        />
    )
}) /* =============================================================================================================== */
