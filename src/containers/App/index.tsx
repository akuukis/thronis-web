import React, { useEffect } from 'react'

import { Box, Toolbar } from '@material-ui/core'
import { useLocation, useNavigate, useOutlet } from 'react-router-dom'

import { createFC } from 'src/common'
import { ROUTE } from 'src/constants'

import Analytics from './Analytics'
import Header from './Header'

// eslint-disable-next-line import/no-unassigned-import
import './index.css'


export default createFC(import.meta.url)(({ children, theme }) => {
    const outlet = useOutlet()
    const navigate = useNavigate()

    // Reset scroll on top to navigation.
    const { pathname } = useLocation()
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [pathname])

    // Redirect 404s.
    useEffect(() => {
        if (!outlet) navigate(ROUTE.CURRENT)
    }, [outlet])

    return (
        <Box sx={{ position: 'relative', minHeight: '100vh' }}>
            <Header />
            <Toolbar sx={{ display: { xs: 'none', sm: 'flex' } }} />
            {outlet}
            <Toolbar sx={{ display: { xs: 'flex', sm: 'none' } }} />
            <Analytics />
        </Box>
    )
}) /* =============================================================================================================== */
