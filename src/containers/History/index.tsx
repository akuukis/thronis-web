import React from 'react'

import { Grid } from '@material-ui/core'

import { createFC } from 'src/common'
import Container from 'src/layouts/DefaultLayout/Container'

import { HISTORY_ITEMS } from './common'
import HIstoryItem from './HIstoryItem'


interface IProps {
}

export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    return (
        <Container
            overline="Vēsture"
            title="Iepriekšējie Troņi"
            description="Pirmais Tronis notika 2013. gadā, pēdējais - šogad. Organizēšanas stafete ir nodota jau trešajai veidotāju komandai"
        >
            <Grid container spacing={4}>
                {HISTORY_ITEMS.map((historyItem) => (
                    <Grid key={historyItem.title} item>
                        <HIstoryItem event={historyItem} />
                    </Grid>
                ))}
            </Grid>
        </Container>
    )
}) /* =============================================================================================================== */
