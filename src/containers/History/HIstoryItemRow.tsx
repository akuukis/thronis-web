import * as React from 'react'

import { Grid, Typography } from '@material-ui/core'

import { createFC } from 'src/common/createFC'
import { UniversalLink } from 'src/components/UniversalLink'

import { HistorySubitem } from './common'


export interface IEvents {
    heading: string,
    date?: string,
    time: string,
    paragraph: string,
}
interface Props {
    icon: JSX.Element
    prefix: string
    subitems?: HistorySubitem[];
}

export default createFC(import.meta.url)<Props>(({ icon, prefix, subitems, theme }) => {
    if (!subitems) return null

    return (
        <Grid container spacing={1}>
            <Grid item>{icon}</Grid>
            <Grid item>
                <Typography>
                    {prefix}
                    &nbsp;
                    {subitems.map((subitem, i, array) => (
                        <>
                            <UniversalLink href={subitem.url} blank>{subitem.title}</UniversalLink>
                            {i !== array.length -1 ? ' / ' : ''}
                        </>
                    ))}
                </Typography>

            </Grid>
        </Grid>
    )
})
