import * as React from 'react'

import { Box, Card, CardContent, Grid, Typography } from '@material-ui/core'

import { useBreakpoint } from 'src/common'
import { createFC } from 'src/common/createFC'
import IconFacebook from 'src/components/icons/IconFacebook'
import IconFlickr from 'src/components/icons/IconFlickr'
import IconPdf from 'src/components/icons/IconPdf'
import IconYoutube from 'src/components/icons/IconYoutube'
import Section from 'src/layouts/DefaultLayout/Section'

import { HistoryItem } from './common'
import HIstoryItemRow from './HIstoryItemRow'


export interface IEvents {
    heading: string,
    date?: string,
    time: string,
    paragraph: string,
}
interface Props {
    event: HistoryItem;
}

export default createFC(import.meta.url)<Props>(({ event, theme }) => {
    const mdUp = useBreakpoint('md')
    return (
        <Card
            variant="elevation"
            sx={{
                [theme.breakpoints.down('md')]: {
                    margin: theme.spacing(0, -2),
                },
            }}
        >
            <CardContent
                sx={{
                    padding: theme.spacing(2,2,0,2),
                    [theme.breakpoints.up('md')]: {
                        padding: theme.spacing(4, 4, 2, 4),
                    },
                    '&:last-child': {
                        paddingBottom: theme.spacing(0),
                    },
                }}
            >
                <Section
                    id={event.title}
                    heading={event.title}
                    overline={event.overline}
                >
                    <Grid container spacing={mdUp ? 4 : 2} justifyContent='center'>
                        <Grid item xs={12} md={6}>
                            {event.text.split('\n').map((paragraph, i) => (
                                <Typography key={i} paragraph>{paragraph}</Typography>
                            ))}
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <HIstoryItemRow icon={<IconFlickr />} prefix='Galerijas:' subitems={event.galleries} />
                            <HIstoryItemRow icon={<IconYoutube />} prefix='Video:' subitems={event.videos} />
                            <HIstoryItemRow icon={<IconFacebook />} prefix='Facebook:' subitems={event.facebooks} />
                            <HIstoryItemRow icon={<IconPdf />} prefix='Noteikumi:' subitems={event.rules} />
                        </Grid>
                        <Grid
                            component="figure"
                            item
                            xs={12}
                            md
                            sx={{
                                overflow: 'hidden',
                            }}
                        >
                            <Box
                                component='img'
                                alt={event.previewImage.title}
                                src={event.previewImage.url}
                                sx={{
                                    display: 'block',
                                    objectFit: 'cover',
                                    objectPosition: 'top',
                                    width: '100%',
                                    height: 420,
                                    marginBottom: theme.spacing(0.5),
                                }}
                            />
                            <Typography component='figcaption' variant='caption' align='center'>{event.previewImage.title}</Typography>
                        </Grid>
                        {event.mapImage?.title && (
                            <Grid
                                component="figure"
                                item
                                xs={12}
                                md={false}
                                sx={{
                                    maxWidth: theme.spacing(event.mapImage ? 60 : 120),
                                    [theme.breakpoints.up('md')]: {
                                        flexBasis: 'auto',
                                        maxWidth: theme.spacing(60),
                                    },
                                }}
                            >
                                <Box
                                    component='img'
                                    alt={event.mapImage.title}
                                    src={event.mapImage.url}
                                    sx={{
                                        display: 'block',
                                        objectFit: 'contain',
                                        objectPosition: 'top',
                                        width: '100%',
                                        height: mdUp ? 420 : undefined,
                                        marginBottom: theme.spacing(0.5),
                                    }}
                                />
                                <Typography component='figcaption' variant='caption' align='center'>{event.mapImage.title}</Typography>
                            </Grid>
                        )}
                    </Grid>
                </Section>
            </CardContent>
        </Card>
    )
})
