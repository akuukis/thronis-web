import throne_10_map from 'assets/history/throne_10_map.png'
import throne_10_preview from 'assets/history/throne_10_preview.jpg'
import throne_1_map from 'assets/history/throne_1_map.jpg'
import throne_1_preview from 'assets/history/throne_1_preview.jpg'
import throne_5_map from 'assets/history/throne_5_map.png'
import throne_5_preview from 'assets/history/throne_5_preview.jpg'
import throne_6_map from 'assets/history/throne_6_map.png'
import throne_6_preview from 'assets/history/throne_6_preview.png'
import throne_7_map from 'assets/history/throne_7_map.png'
import throne_7_preview from 'assets/history/throne_7_preview.jpg'
import throne_8_map from 'assets/history/throne_8_map.png'
import throne_8_preview from 'assets/history/throne_8_preview.jpg'
import throne_9_map from 'assets/history/throne_9_map.png'
import throne_9_preview from 'assets/history/throne_9_preview.jpg'
import throne_mini2_map from 'assets/history/throne_mini2_map.png'
import throne_mini2_preview from 'assets/history/throne_mini2_preview.jpg'
import throne_mini_map from 'assets/history/throne_mini_map.png'
import throne_mini_preview from 'assets/history/throne_mini_preview.jpg'
import rules_summaryMini2020 from 'assets/rules/Mini-Throne-2020-Rules-summary.pdf'
import rules_Mini2020 from 'assets/rules/Mini-Throne-2020-Rules.pdf'
import rules_appendixMini2022 from 'assets/rules/Mini-Throne-2022-Rules-appendix.pdf'
import rules_summaryMini2022 from 'assets/rules/Mini-Throne-2022-Rules-summary.pdf'
import rules_Mini2022 from 'assets/rules/Mini-Throne-2022-Rules.pdf'
import rules_appendixIX from 'assets/rules/Throne-IX-Rules-appendix.pdf'
import rules_summaryIX from 'assets/rules/Throne-IX-Rules-summary.pdf'
import rules_IX from 'assets/rules/Throne-IX-Rules.pdf'
import rules_V from 'assets/rules/Throne-V-Rules.pdf'
import rules_summaryVI from 'assets/rules/Throne-VI-Rules-summary.pdf'
import rules_VI from 'assets/rules/Throne-VI-Rules.pdf'
import rules_summaryVII from 'assets/rules/Throne-VII-Rules-summary.pdf'
import rules_VII from 'assets/rules/Throne-VII-Rules.pdf'
import rules_appendixVIII from 'assets/rules/Throne-VIII-Rules-appendix.pdf'
import rules_summaryVIII from 'assets/rules/Throne-VIII-Rules-summary.pdf'
import rules_VIII from 'assets/rules/Throne-VIII-Rules.pdf'
import rules_summaryX from 'assets/rules/Throne-X-Rules-summary.pdf'
import rules_X from 'assets/rules/Throne-X-Rules.pdf'
import rules_appendixX from 'assets/rules/Throne-X-Rules.pdf'


export interface HistorySubitem {
    title: string
    url: string
}

export interface HistoryItem {
    title: string
    overline?: string
    text: string
    previewImage: HistorySubitem
    mapImage?: HistorySubitem
    rules?: HistorySubitem[]
    galleries?: HistorySubitem[]
    facebooks?: HistorySubitem[]
    videos?: HistorySubitem[]
}

export const HISTORY_ITEMS: HistoryItem[] = [
    {
        title: 'Tronis X',
        overline: 'Suntaži, 2023. 11.-12. augusts',
        text: 'Desmit gadu jubilejas Tronis Axela un Elona vadībā. Komandas piedalījās mistērijām apvītās dzīrēs un nenogurdams cīnījās par varu jaunajās Purzemes teritorijās. Spraiga cīņā, punkts punktā ar Lūšiem un Čūskām, Lāči, Aivvv vadībā izcīnīja pelnītu uzvaru.\nSpēlē piedalījās 3 komandas: Lāči, Lūši, Čūskas \nUzvarētāji: Lāči Aivvv vadībā',
        previewImage: { title: 'Dzīru laikā nodarboties ar kaitniecību nedrīks', url: throne_10_preview },
        mapImage: { title: 'Tronis X karte', url: throne_10_map },
        rules: [
            { title: 'Summary', url: rules_summaryX },
            { title: 'Full', url: rules_X },
            { title: 'Appendix', url: rules_appendixX },
        ],
        galleries: [
            { title: 'Annija', url: 'https://www.flickr.com/photos/198926924@N07/albums/72177720310462924/' },           
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/788315866242741' },
        ],
    },
    {
        title: 'Tronis IX',
        overline: 'Silciems, 2022. 12.-13. augusts',
        text: 'Kalniņu ēras kulminācija - spraiga un episka cīņa par uzvaru visas spēles garumā līdz pat pēdējai kaujai. Pirmā lietuviešu komanda Troņa vēsturē radīja bailes no Čūsku bultām katru reizi izejot no bāzes.\nSpēlē piedalījās 6 komandas: Lāči, Vilki, Lūši, Jenoti, Čūskas, Odi\nUzvarētāji: Vilki Aksela vadībā',
        previewImage: { title: 'Vilki kopā ar noalgotajiem Bandītiem bēg no Čūskām un Lūšiem.', url: throne_9_preview },
        mapImage: { title: 'Tronis VII karte', url: throne_9_map },
        rules: [
            { title: 'Summary', url: rules_summaryIX },
            { title: 'Full', url: rules_IX },
            { title: 'Appendix', url: rules_appendixIX },
        ],
        galleries: [
            { title: 'Annija', url: 'https://flic.kr/s/aHBqjA36pd' },
            { title: 'Spēlētāji', url: 'https://flic.kr/s/aHBqjA3hJo' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/1388414654944787' },
        ],
    },
    {
        title: 'mini Tronis 2',
        overline: 'Lilaste, 2022. 14. maijs',
        text: 'Īsa, intensīva dienas spēle kalnainā un ainaviskā apvidū, lai pārliecinātos, ka tehnika ir perfektā kārtībā.\nSpēlē piedalījās 4 komandas: Lāči, Vilki, Lūši, Aligatori\nUzvarētāji: Lūši Ernesta Auziņa vadībā',
        previewImage: { title: 'Jenotu un Lūšu komandas dejo auglības deju.', url: throne_mini2_preview },
        mapImage: { title: 'Tronis VII karte', url: throne_mini2_map },
        rules: [
            { title: 'Summary', url: rules_summaryMini2022 },
            { title: 'Full', url: rules_Mini2022 },
            { title: 'Appendix', url: rules_appendixMini2022 },
        ],
        galleries: [
            { title: 'Mārtiņš', url: 'https://flic.kr/s/aHBqjzR7Ri' },
            { title: 'Aivis', url: 'https://flic.kr/s/aHBqjzTwcF' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/485251333304616' },
        ],
    },
    {
        title: 'Tronis VIII',
        overline: 'Silciems, 2021. gada 13. - 14. augusts',
        text: 'Episks pasākums gan pēc improvizētā džeza koncerta Dzīrēs, gan pilnīgi jaunās sistēmas spēlē. Kad dienas vidū viss saplīsa, turpinājām ar lootfest.\nSpēlē piedalījās 6 komandas: Lāči, Vilki, Lūši, Jenoti, Mērkaķi, Salamandras\nUzvarētāji: Lāči Ances Zvejas vadībā',
        previewImage: { title: 'Episka lielā kauja, kurā komandas visilgāk cīnījās par Troņa karoga turētāju.', url: throne_8_preview },
        mapImage: { title: 'Tronis VII karte', url: throne_8_map },
        rules: [
            { title: 'Summary', url: rules_summaryVIII },
            { title: 'Full', url: rules_VIII },
            { title: 'Appendix', url: rules_appendixVIII },
        ],
        galleries: [
            { title: 'Mārtiņš', url: 'https://flic.kr/s/aHsmWv6yMT' },
            { title: 'Annija', url: 'https://flic.kr/s/aHsmWqxv6f' },
            { title: 'Aivis', url: 'https://flic.kr/s/aHsmWyUUQY' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/853899235209439' },
        ],
    },
    {
        title: 'Tronis VII',
        overline: 'Silciems, 2020. gada 14. - 15. augusts',
        text: '5 komandas, 56 spēlētāji, episkas Dzīres un spēle, kurā Vilki viltīgi apspēlēja pārējās komandas un uz naža asmens izcīnīja uzvaru ar lielu pārsvaru.\nSpēlē piedalījās 4 komandas: Lāči, Vilki, Lūši, Kraukļi\nUzvarētāji: Vilki Aksela vadībā',
        previewImage: { title: 'Lāču un Vilku troņmantinieki diplomātiski sarunā Vilkiem izdevīgu darījumu.', url: throne_7_preview },
        mapImage: { title: 'Tronis VII karte', url: throne_7_map },
        rules: [
            { title: 'Summary', url: rules_summaryVII },
            { title: 'Full', url: rules_VII },
        ],
        galleries: [
            { title: 'Mārtiņš', url: 'https://flic.kr/s/aHsmQ5vj1r' },
            { title: 'Aivis', url: 'https://flic.kr/s/aHsmQ5wQhB' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/513566326134289' },
        ],
    },
    {
        title: 'mini Tronis',
        overline: 'Babelītis, 2020. gada 6. - 7. februāris',
        text: '"Man patīk Tronis, bet es pats vairs netieku pie spēlēšanas."\nDzimšanas dienas dāvana galvenajam orgam jeb Tronis, ko viņš pats neorganizē. Īsa 4 stundu spēles versija ap Bābelītes ezeru tikai ar pašu galveno!\nSpēlē piedalījās 3 komandas: Lūši, Kraukļi, Lāči (?)\nUzvarētāji: Lūši Andreja Kļaviņa vadībā',
        previewImage: { title: 'Jubilārs apmāca komandas biedrenes LARP zobencīņā.', url: throne_mini_preview },
        mapImage: { title: 'mini Tronis karte', url: throne_mini_map },
        rules: [
            { title: 'Summary', url: rules_summaryMini2020 },
            { title: 'Full', url: rules_Mini2020 },
        ],
        galleries: [
            { title: 'Annija', url: 'https://flic.kr/s/aHsmLhMc1G' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/3036541269698303' },
        ],
    },
    {
        title: 'Tronis VI',
        overline: 'Silciems, 2019. gada 14.-15. augusts',
        text: 'Izdevies pasākums, ar 20+ dalībniekiem. Spēle, kurā viss iet kā pa sviestu.\nSpēlē piedalījās 4 komandas: Lūši, Lāči, Eži, Vanagi\nUzvarētāji: Lūši Andreja Kļaviņa vadībā',
        previewImage: { title: 'Tronis VI klejojošā pirātu komanda uz kuģa.', url: throne_6_preview },
        mapImage: { title: 'Tronis VI karte', url: throne_6_map },
        rules: [
            { title: 'Summary', url: rules_summaryVI },
            { title: 'Full', url: rules_VI },
        ],
        galleries: [
            { title: 'Mārtiņš', url: 'https://flic.kr/s/aHsmGjM5Dy' },
        ],
        videos: [
            { title: 'Lielās kaujas video', url: 'https://youtu.be/b9LryG0dMDI' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/344378273155766' },
        ],
    },
    {
        title: 'Tronis V',
        overline: 'Silciems, 2018. gada 2.-3. novembris',
        text: 'Pirmā spēle orgiem. Sapņi augsti, negulētas nedēļas daudz, pasākums notika, bildes maģiskas.\nSpēlē piedalījās 4 komandas: Vilki, Lāči, Āpši, Lūši \nUzvarētāji: Vilki Jāņa Siliņa vadībā',
        previewImage: { title: 'Kāds no Sesku komandas reiz pateica "paturi karti" un komandas pieredzējušākais spēlētājs to arī darīja. Visu spēli.', url: throne_5_preview },
        mapImage: { title: 'Tronis V karte', url: throne_5_map },
        rules: [
            { title: 'Full', url: rules_V },
        ],
        galleries: [
            { title: 'Mārtiņš', url: 'https://flic.kr/s/aHsmmrUU9W' },
        ],
        facebooks: [
            { title: 'Pasākums', url: 'https://www.facebook.com/events/328000004621264/' },
        ],
    },
    {
        title: 'Tronis I - IV',
        text: 'Pirmos Troņus organizēja citi organizatori - Kamazs un Aksels. Taču Kamazam notika dzīve, piedzima bērni un apnika taisīt LARPus.\nTronis I (2013) - 3 komandas: Vilki (uzvarētāji), Lāči, Brieži\nTronis II (2014) - 3 komandas: Vilki (uzvarētāji), Lāči, Brieži\nTronis III (2015) - 4 komandas: Vilki (uzvarētāji), Lāči, Brieži, Lūši\nTronis IV (2016) - 5 komandas: Lūši(uzvarētāji), Vilki, Lāči, Brieži, Ūdri',
        previewImage: { title: 'Kad dzīve bija skaista un vienkārša', url: throne_1_preview },
        mapImage: { title: '2013. gada spēles karte', url: throne_1_map },
        facebooks: [
            { title: 'Grupa 2015', url: 'https://www.facebook.com/groups/466631286848556' },
            { title: 'Grupa 2016', url: 'https://www.facebook.com/groups/577291745664701' },
        ],
    },
]
