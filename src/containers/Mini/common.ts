import { IEvents } from 'src/components/EventCard'


export const EVENTS: IEvents[] = [
    {
        heading: 'Reģistrācija',
        time: '09.00 - 11.00',
        paragraph: 'Ierašanās un sagatavošanās spēlei - dalībnieku reģistrācija, ieroču pārbaude un spēlētāju instruktāža.',
    },
    {
        heading: 'Spēle',
        time: '11.00 - 16.00',
        paragraph: 'Komandu cīņa par labākās titulu iekaustot pretiniekus, vācot resursus un veidojot diplomātiskas attiecības ar konkurentiem.',
    },
    {
        heading: 'Lielā kauja',
        time: '16.00 - 17.00',
        paragraph: 'Noslēguma lielā kauja, kurā visas komandas vienuviet cīnās par pēdējiem uzvaras punktiem.',
    },
    {
        heading: 'Afterītis',
        time: '19.30 - ...',
        paragraph: 'Pēcspēles pasēdēšana Hospitāļu Ezītī (Zirņu iela 21). Stāsti un pasakas par dalībnieku spēles iespaidiem.',
    },

]
