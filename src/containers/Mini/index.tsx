import React from 'react'

import { Box, Button, Card, Grid, Icon, Typography } from '@material-ui/core'
import IconFacebook from '@material-ui/icons/Facebook'

import topimg from 'assets/mini/conversation.jpg'
import botimg from 'assets/mini/team.jpg'
import gm_href from 'assets/venue/gm_href.png'
import osm_href from 'assets/venue/osm_href.png'
import waze_href from 'assets/venue/waze_href.png'


import { createFC, useBreakpoint } from 'src/common'
import { ContactCard } from 'src/components/ContactCard'
import { EventCard } from 'src/components/EventCard'
import { Table } from 'src/components/Table'
import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

import { CONTACTS } from '../Contacts/common'

import { EVENTS } from './common'
import map from './karte.png'


export default createFC(import.meta.url)(({ theme }) => {
    const lgUp = useBreakpoint('lg')

    return (
        <Container
            overline="Spēle 2022. gada 14. maijā"
            title="Mazais Tronis"
            description="Zini, kas notiks un ierodies pareizajā vietā!"
        >
            <Section heading="Laiks un vieta">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6} sx={{ overflow: 'hidden' }}>
                        <Box
                            component="img"
                            src={map}
                            alt='karte'
                            sx={{ minHeight: 300, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'center' }}
                        />
                    </Grid>
                    {/* <Grid item xs={12} lg={6} sx={{ [theme.breakpoints.down('sm')]: { display: 'none' } }}> */}
                    {/* <Box
                            component='iframe'
                            width="425"
                            height="300"
                            scrolling="no"
                            src="https://www.openstreetmap.org/export/embed.html?bbox=24.33302164077759%2C57.164441645548386%2C24.37492847442627%2C57.17908719479471&amp;layer=mapnik&amp;marker=57.17176514550133%2C24.35397505760193"
                            sx={{ minHeight: 300, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'top' }}
                        />
                    </Grid> */}
                    <Grid item xs={12} lg={6} alignItems="center" sx={{ display: 'flex', alignItems: 'center' }}>
                        <Card sx={{ background: theme.palette.background.default, [theme.breakpoints.up('sm')]: { padding: theme.spacing(4, 5) }  }}>
                            <Typography align="center" variant="h3" sx={{ marginBottom: 2 }}>Sestdiena, 14. maijs</Typography>
                            <Table
                                sx={{ marginBottom: 4 }}
                                columns={['Reģistrācija', 'Spēle', 'Lielā kauja']}
                                rows={[['9.00', '11.00', '16.00']]}
                                bold
                            />
                            <Typography align="center" variant="h3" sx={{ marginBottom: 2 }}>
                                Lilaste, Ādažu novads
                            </Typography>
                            <Typography align="center" paragraph>
                                Mazais Tronis notiek 30ha spēles poligonā Latvijas Valsts mežu teritorijā, Lilastes ezera dienvidrietumu pussalā.
                            </Typography>
                            <Typography align="center">
                                <UniversalLink href="https://fb.me/e/1NG1QV7oF" title="facebook" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <IconFacebook />
                                    </Icon>
                                    {' '}
                                    Facebook pasākums
                                </UniversalLink>
                            </Typography>
                        </Card>

                    </Grid>
                </Grid>
            </Section>
            <Section heading="Transports">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}>
                        <Card sx={{
                            padding: theme.spacing(5.25),
                            background: theme.palette.background.paper,
                            height: '100%',
                        }}
                        >
                            <Typography> Uz spēles poligonu var nokļūt gan ar paša, gan ar sabiedrisko transportu ar: </Typography>
                            <ul>
                                <li>
                                    <Typography>
                                        🚈&nbsp;&nbsp;
                                        <UniversalLink href="https://www.pv.lv/lv/marsrutu-saraksts/?from=R%C4%ABga&stop_from=1&to=Lilaste&stop_to=306&date=14.05.2022" title="vilcienu" blank>
                                            vilcienu
                                        </UniversalLink>
                                        {' '}līdz Lilastes stacijai
                                    </Typography>
                                </li>
                                <li>
                                    <Typography>
                                        🚌&nbsp;&nbsp;
                                        <UniversalLink href="https://www.1188.lv/satiksme/saraksti/riga/lilaste/200001/103983/diena/2022-05-14" title="autobusu" blank>
                                            autobusu
                                        </UniversalLink>
                                        {' '} līdz Lilastes pieturai
                                    </Typography>
                                </li>
                            </ul>
                            <Typography> Poligons atrodas 1-1.5 km attālumā no sabiedrisko transportu pieturām, tātad ierēķini ~20min ko iet ar kājām. </Typography>
                            <Typography sx={{ marginTop: 2, marginBottom: 2 }}>
                                <Typography>
                                    Braucot ar 🚗&nbsp;&nbsp;auto vari piebraukt līdz pašai reģistrācijai pa Latvijas Valsts Mežu labi uzturētu ceļu.
                                    (Nebrauc no Z puses - tur var iestrēgt smiltīs!)
                                </Typography>
                            </Typography>
                            <Typography>
                                <UniversalLink href="https://www.openstreetmap.org/?mlat=57.1718&mlon=24.3540#map=16/57.1718/24.3540" title="OpenStreetMap" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <img src={osm_href} alt='OpenStreetMap' />
                                    </Icon>
                                    {' '}OpenStreetMap
                                </UniversalLink>
                            </Typography>
                            <Typography>
                                <UniversalLink href="https://ul.waze.com/ul?ll=57.17154994%2C24.35372829&navigate=yes" title="Waze" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <img src={waze_href} alt='Waze norādes' />
                                    </Icon>
                                    {' '}Waze
                                </UniversalLink>
                            </Typography>
                            <Typography>
                                <UniversalLink href="https://goo.gl/maps/qb6wCcBMpK24aqCs7" title="Google Maps" blank>
                                    <Icon sx={{ verticalAlign: 'text-bottom', img: { width: '100%' } }}>
                                        <img src={gm_href} alt='Google Maps' />
                                    </Icon>
                                    {' '}Google Maps
                                </UniversalLink>
                            </Typography>
                        </Card>

                    </Grid>
                    <Grid item xs={12} lg={6}>
                        <Box
                            component='iframe'
                            width="425"
                            height="300"
                            src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d138845.25100353005!2d24.08949344800358!3d57.06082970850299!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e0!4m5!1s0x46eecfb0e5073ded%3A0x400cfcd68f2fe30!2zUsSrZ2E!3m2!1d56.9496487!2d24.1051865!4m3!3m2!1d57.1718443!2d24.3531066!5e0!3m2!1sen!2slv!4v1650661371171!5m2!1sen!2slv"
                            sx={{ minHeight: 300, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'top' }}
                        />
                    </Grid>
                </Grid>
            </Section>
            <Section heading="Programma">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}><EventCard event={EVENTS[0]} /></Grid>
                            <Grid item xs={12}><EventCard event={EVENTS[1]} /></Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={6} sx={{ overflow: 'hidden' }} hidden={!lgUp}>
                        <Box
                            component="img"
                            src={topimg}
                            alt='Spēle un Dzīres'
                            sx={{ minHeight: 360, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'top' }}
                        />
                    </Grid>
                </Grid>
                <Grid container direction='row-reverse' spacing={2} sx={{ marginTop: lgUp ? 2 : 0 }}>
                    <Grid item xs={12} lg={6}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}><EventCard event={EVENTS[2]} /></Grid>
                            <Grid item xs={12}><EventCard event={EVENTS[3]} /></Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={6} sx={{ overflow: 'hidden' }}>
                        <Box
                            component="img"
                            src={botimg}
                            alt='Lielā kauja un Afterītis'
                            sx={{ minHeight: 360, width: '100%', height: '100%', aspectRatio: '4', objectFit: 'cover', objectPosition: 'center' }}
                        />
                    </Grid>
                </Grid>
            </Section>
            <Section heading="Kontakti">
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={6}><ContactCard contact={CONTACTS[1]} /></Grid>
                    <Grid item xs={12} lg={6} sx={{ display: 'flex' }}>
                        <Grid container alignItems="center">
                            <Grid item sx={{ marginLeft: 5.25, marginRight: 5.25 }}>
                                <Typography align="center">
                                    Spēles galvenā organzatore ir Rota Kalniņa. Ar viņu vari sazināties jebkurā ar
                                    spēli saistītā sakarā.
                                </Typography>
                                <Typography align="center" sx={{ marginTop: 2 }}>
                                    <Button href={ROUTE.CONTACTS} variant="outlined" sx={{ margin: 1 }}>
                                        Visi organizatori
                                    </Button>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
