import React from 'react'

import { Grid, Typography } from '@material-ui/core'


import { createFC, useBreakpoint } from 'src/common'
import { ClothesCard } from 'src/components/ClothesCard'
import { EquipmentCard } from 'src/components/EquipmentCard'
import { ItemCard } from 'src/components/ItemCard'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

import { CLOTHES } from '../Clothes/common'
import { EQUIPMENT } from '../Equipment/common'

import { ITEMS } from './common'


export default createFC(import.meta.url)(({ theme }) => {
    const isLgUp = useBreakpoint('lg')

    return (
        <Container
            overline="Ko ņemt līdzi"
            title="Mantas"
            description="Neatkarīgi no tā, vai spēlēsi vienu vai vairākas dienas, iepazīsties ar lietām, ko vērts paņemt līdzi uz spēli!"
        >
            <Section heading='Ekipējums'>
                <EquipmentCard
                    item={EQUIPMENT[0]}
                    sx={{ background: 'linear-gradient(154.17deg, #E0E0E0 0%, #FAFAFA 83.69%);' }}
                />
            </Section>
            <Section heading='Apģērbs'>
                <ClothesCard
                    item={CLOTHES[4]}
                    sxCaption={{ color: theme.palette.primary.contrastText }}
                    sx={{ color: isLgUp ? theme.palette.primary.contrastText : theme.palette.text.primary }}
                    sxBox={isLgUp ? {} : { background: theme.palette.primary.contrastText, padding: 0 }}
                />
            </Section>
            <Section heading="Mantas">
                <Grid container spacing={isLgUp ? 4 : 2}>
                    {Object.values(ITEMS.slice(0, 4).map((item, index) => (
                        <Grid key={index} item xs={12} lg={6}>
                            <ItemCard sx={{ borderColor: theme.palette.primary.border }} item={item} />
                        </Grid>
                    )))}
                </Grid>
                <Typography variant='h3' sx={{ marginTop: 6, marginBottom: 4 }}>Nav obligāti, bet noderēs</Typography>
                <Grid container spacing={isLgUp ? 4 : 2}>
                    {Object.values(ITEMS.slice(4,8).map((item, index) => (
                        <Grid key={index} item xs={12} lg={6}>
                            <ItemCard item={item} />
                        </Grid>
                    )))}
                </Grid>
                <Typography variant='h3' sx={{ marginTop: 6, marginBottom: 4 }}>Nakšņošanai</Typography>
                <Grid container spacing={isLgUp ? 4 : 2}>
                    {Object.values(ITEMS.slice(8).map((item, index) => (
                        <Grid key={index} item xs={12} lg={6}>
                            <ItemCard item={item} />
                        </Grid>
                    )))}
                </Grid>
            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
