import { IItem } from 'src/components/ItemCard'

import cup from '/assets/items/cup.png'
import snack from '/assets/items/snack.png'
import hygiene from '/assets/items/hygiene.png'
import medicine from '/assets/items/medicine.png'
import sleepingbag from '/assets/items/sleepingbag.png'
import suncream from '/assets/items/suncream.png'
import tent from '/assets/items/tent.png'
import watch from '/assets/items/watch.png'
import water from '/assets/items/water.png'
import bowl from '/assets/items/bowl.png'


export const ITEMS: IItem[] = [
    {
        image: snack,
        heading: 'Uzkodas',
        paragraph: 'Paņem līdzi uzkodas visai spēles dienai un, ja dosies uz Dzīrēm - mielastu kopējam galdam. Spēlē nodrošināsim zupu Dzīru vakarā, brokastu putru spēles rītā un  iespēju pasūtīt siltas pusdienas no Siguldas Kebaba.',
    },
    {
        image: medicine,
        heading: 'Ikdienas medikamenti',
        paragraph: 'Atceries paņemt savas zāles, ko lieto ikdienā.',
    },
    {
        image: bowl,
        heading: 'Bļoda un karote',
        paragraph: 'Paņem līdzi bļodu un karoti Dzīru mielasta zupai un rīta brokastu putrai.',
    },
    {
        image: cup,
        heading: 'Krūze vai kauss',
        paragraph: 'No sava kausa vai krūzes Dzīru vakara dzērieni no kopīgā mielasta galda garšo labāk!',
    },
    {
        image: watch,
        heading: 'Rokaspulkstenis',
        paragraph: 'Rokaspulkstenis palīdzēs sekot līdzi, lai netīšām nenokavē ko svarīgu, jo aizgulējies (un īpaši noderēs stjuartiem).',
    },
    {
        image: hygiene,
        heading: 'Higiēnas preces',
        paragraph: 'Būs tualete ar tualetes papīru un iespēju nomazgāt rokas. Dāmas aicinām paņemt savas higiēnas preces!',
    },
    {
        image: water,
        heading: 'Ūdens',
        paragraph: 'Katrai komandai nodrošinām 2l uz cilvēku, bet noteikti paņem līdzi savu pudeli un ūdeni, ja ar 2l tev nepietiks.',
    },
    {
        image: suncream,
        heading: 'Krēmi pret mošķiem un sauli',
        paragraph: 'Līdzeklis pret kukaiņiem, pretapdeguma vai sauļošanās krēms mēdz būt labi palīgi atpūtai dabā.',
    },
    {
        image: tent,
        heading: 'Telts',
        paragraph: 'Lai nav jāguļ plikās sūnās, nodrošinies pret nokrišņiem naktī un paņem telti. Brīdinu, ka guļamzona ir diezgan smilšaina un pilna ar viršiem.',
    },
    {
        image: sleepingbag,
        heading: 'Guļammaiss',
        paragraph: 'Pa nakti augustā mēdz būt zem 10°C, bez guļammaisa nekādi!',
    },

]
