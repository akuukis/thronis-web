import clothes from '/assets/clothes/fantasytrash.png'
import availableequipment from '/assets/clothes/availableequipment.png'
import secondhand from '/assets/clothes/secondhand.png'
import wardrobe from '/assets/clothes/wardrobe.png'

import { Typography } from '@material-ui/core'

import React from 'react'

import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'


export interface IClothes {
    heading?: string
    image: string,
    caption: string,
    content?: React.ReactNode[] | JSX.Element,
    button?: string,
    buttonHref?: string,
}

export const CLOTHES: IClothes[] = [
    {
        image: clothes,
        caption: 'Piemērs: Tronis VI dalībnieki savos spēles tērpos',
        content: (<>
            <Typography paragraph>
                Troņa tematika ir <em>medieval fantasy trash LARP.</em>
            </Typography>
            <Typography paragraph>
                Tas nozīmē, ka uz spēli ģerbjam viduslaiku tematikas drēbes, bet par vēsturisko precizitāti un elegantumu pārāk neiespringstam.
            </Typography>
        </>),
    },
    {
        image: availableequipment,
        caption: 'Tronis VII organizatoru ieroču čupa',
        content: (<>
            <Typography paragraph>
                2020. gadā uz Tronis VII spēli organizatori un dalībnieki uztaisīja
                <strong>{' '}~ 50{' '}</strong>
                īso, bastarda un garo zobenu.
                Šie ieroči ir pieejami spēlētājiem un tiks sadalīti pa komandām.
                Tā kā ja tev nav sava ieroča, mēs varam aizdot uz spēli!
            </Typography>
        </>),
    },
    {
        image: secondhand,
        caption: 'Dalībnieka atradums second hand veikalā uz Tronis VII',
        heading: 'Otrā Elpa 💚',
        content: (<Typography paragraph>
            Jau LARP lietošanai gatavus tērpus ļoti bieži var atrast second hand veikalos.
            Ja Tavs tērps vēl nav gatavs, dodies uz <UniversalLink href='https://otraelpa.lv' blank>Otrā Elpa</UniversalLink> un
            droši jautā pārdevējai pēc Troņa maisa - tur atliktas ādas un mākslīgās kažokādas par simbolisku samaksu.
        </Typography>),
    },
    {
        image: wardrobe,
        caption: 'Suntažu garderobe, kas paņemta līdzi uz Tronis VI',
        heading: 'Suntažu garderobe',
        content: (<>
            <Typography paragraph>
                Aksela mājas Suntažos ir Latvijas LARP centrālais štābs, kurā ir gan LARP ekipējuma, gan apģērba krātuve.
            </Typography>
            <Typography paragraph>
                Ik gadu Aksels atved daļu no krājumiem uz Troni. Ja līdz spēlei tavs tērps nav pilnīgs,
                esi laipni gaidīts spēles Dzīru vakarā ieskatīties Suntažu garderobē un ko aizņemties!
            </Typography>
        </>),
    },
    {
        image: clothes,
        caption: 'Piemērs: Tronis VI dalībnieki savos spēles tērpos',
        content: (<>
            <Typography paragraph>
                Troņa tematika ir <em>medieval fantasy trash LARP.</em>
            </Typography>
            <Typography paragraph>
                Tas nozīmē, ka uz spēli ģerbjam viduslaiku tematikas drēbes, bet par vēsturisko precizitāti un elegantumu pārāk neiespringstam.
            </Typography>
        </>),
        button: 'Viss par apģērbu',
        buttonHref: ROUTE.CLOTHES,
    },
]
