import React from 'react'

import { Card, CardContent, Grid, List, ListItem, Typography } from '@material-ui/core'


import { createFC, useBreakpoint } from 'src/common'
import { ClothesCard } from 'src/components/ClothesCard'
import Container from 'src/layouts/DefaultLayout/Container'


import { CLOTHES } from './common'
import IconBag from './Icons/IconBag'
import IconBear from './Icons/IconBear'
import IconBelt from './Icons/IconBelt'
import IconCheck from './Icons/IconCheck'
import IconClose from './Icons/IconClose'
import IconClothes from './Icons/IconClothes'
import IconShoe from './Icons/IconShoe'


export default createFC(import.meta.url)(({ theme }) => {
    const isLgUp = useBreakpoint('lg')

    return (
        <Container
            overline="Ko vilkt mugurā"
            title="Apģērbs"
            description="Esi skatījies kādu lētu viduslaiku vai fantāzijas filmu? Iedomājies fona aktieri tādā filmā. Ģērbies tā."
        >
            <ClothesCard item={CLOTHES[0]} sxBox={{ background: theme.palette.background.default }} />
            <Grid container sx={{ marginTop: 8 }} spacing={4}>
                <Grid item xs={12} lg={6}>
                    <Card sx={{ padding: isLgUp ? 3 : 0, height: '100%' }}>
                        <CardContent>
                            <Typography variant='h3'>Vērts ievērot:</Typography>
                            <Grid container>
                                <Grid item xs={6}>
                                    <List sx={{ li: { paddingLeft: 0 } }}>
                                        <ListItem sx={{ display: 'block' }}><IconCheck color='inherit' fontSize='inherit' />&nbsp;neitrāli apavi</ListItem>
                                        <ListItem sx={{ display: 'block' }}><IconCheck color='inherit' fontSize='inherit' />&nbsp;auduma bikses</ListItem>
                                        <ListItem sx={{ display: 'block' }}><IconCheck color='inherit' fontSize='inherit' />&nbsp;dabiski materiāli</ListItem>
                                        <ListItem sx={{ display: 'block' }}><IconCheck color='inherit' fontSize='inherit' />&nbsp;pogas un auklas</ListItem>
                                    </List>
                                </Grid>
                                <Grid item xs={6}>
                                    <List sx={{ li: { paddingLeft: 0 } }}>
                                        <ListItem sx={{ display: 'block' }}><IconClose color='inherit' fontSize='inherit' />&nbsp;spilgtas kedas</ListItem>
                                        <ListItem sx={{ display: 'block' }}><IconClose color='inherit' fontSize='inherit' />&nbsp;džinsi</ListItem>
                                        <ListItem sx={{ display: 'block' }}><IconClose color='inherit' fontSize='inherit' />&nbsp;lieli uzraksti</ListItem>
                                        <ListItem sx={{ display: 'block' }}><IconClose color='inherit' fontSize='inherit' />&nbsp;redzami rāvējslēdzēji</ListItem>
                                    </List>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} lg={6}>
                    <Card sx={{ padding: isLgUp ? 3 : 0, height: '100%' }}>
                        <CardContent>
                            <Typography variant='h3'>Iesakām:</Typography>
                            <List sx={{ li: { paddingLeft: 0 } }}>
                                <ListItem sx={{ display: 'block' }}><IconBag color='inherit' fontSize='inherit' />&nbsp;soma vai auduma/ādas maisiņš (pouch)</ListItem>
                                <ListItem sx={{ display: 'block' }}><IconBelt color='inherit' fontSize='inherit' />&nbsp;josta, kur piestiprināt ieroci u.c. sīkumus</ListItem>
                                <ListItem sx={{ display: 'block' }}><IconShoe color='inherit' fontSize='inherit' />&nbsp;drēbes un apavus, ar kuriem ērti kustībā</ListItem>
                                <ListItem sx={{ display: 'block' }}><IconBear color='inherit' fontSize='inherit' />&nbsp;kažokādas (gan mākslīgās, gan dabiskās)</ListItem>
                                <ListItem sx={{ display: 'block' }}><IconClothes color='inherit' fontSize='inherit' />&nbsp;apģērbs, kas noklāj visu augumu</ListItem>
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <ClothesCard item={CLOTHES[2]} sx={{ marginTop: isLgUp ? 9 : 4 }} sxBox={{ background: theme.palette.background.default }} />
            <ClothesCard item={CLOTHES[3]} sx={{ marginTop: isLgUp ? 9 : 4 }} sxBox={{ background: theme.palette.background.default }} left={false} />
        </Container>
    )
}) /* =============================================================================================================== */
