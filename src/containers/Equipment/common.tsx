import sword1 from '/assets/equipment/sword1.png'
import sword2 from '/assets/equipment/sword2.png'
import sword3 from '/assets/equipment/sword3.png'
import dagger from '/assets/equipment/dagger.png'
import bow from '/assets/equipment/bow.png'
import shield from '/assets/equipment/shield.png'
import buckler from '/assets/equipment/buckler.png'
import pouch from '/assets/equipment/pouch.png'
import armor from '/assets/equipment/armor.png'
import ram from '/assets/equipment/ram.png'

import { List, ListItem, Typography } from '@material-ui/core'

import React from 'react'

import { TableProps } from 'src/components/Table'


export interface IEquipment {
    image: string,
    table?: TableProps,
    caption?: string,
    button?: string,
    href?: string,
    heading?: string,
    content?: JSX.Element
}

export const EQUIPMENT: IEquipment[] = [
    {
        image: sword1,
        table: { columns: ['garums', 'asmens', 'rokturis'], rows: [['līdz 100 cm', 'līdz 75cm', 'līdz 15 cm']] },
        button: 'Viss par ekipējumu',
        href: '/equipment',
        content: (<Typography>
            Atšķirībā no citiem LARP, Tronī <strong>katram dalībniekam ir ierocis</strong>, kas pārsvarā ir īsais zobens ⚔️.
            Taču tas nav vienīgais ekipējuma veids, ko var ņemt līdzi vai aizņemties no organizatoriem.
        </Typography>),
    },
    {
        image: dagger,
        table: { columns: ['garums', 'asmens', 'rokturis'], rows: [['18-30 cm', '5-15 cm', '5-15 cm']] },
        heading: 'Duncis',
        // caption: 'Taisīt savu dunci',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            Veikalā pirkts speciāls LARP duncis, vai paštaisīts:
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>Materiāls: siltumizolācija vai latekss.</ListItem>
                <ListItem>Drīkst mest</ListItem>
                <ListItem>
                    Nedrīkst sastāvēt no metāla, koka u.c. blīviem materiāliem, lai mešanas gadījumā
                    neradītu miesas bojājumus.
                </ListItem>
            </List>
        </Typography>),
    },
    {
        image: sword1,
        table: { columns: ['garums', 'asmens', 'rokturis'], rows: [['līdz 100 cm', 'līdz 75cm', 'līdz 15 cm']] },
        heading: 'Īsais zobens',
        // caption: 'Taisīt savu īso zobenu',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            Veikalā pirkts speciāls LARP īsais zobens, vai paštaisīts:
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>Lietojams vienā rokā</ListItem>
                <ListItem>Ar ne-šķīstošu serdeni (bambuss nederēs)</ListItem>
                <ListItem>
                    Mīkstināts no visām asmeņa un roku sarga (guard) pusēm un arī gala.
                    Mīkstumu pārbauda iesitot īpašniekam un paskatoties viņa reakciju.
                </ListItem>
                <ListItem>Nav "bezsvara" smagumā</ListItem>
            </List>
        </Typography>),
    },
    {
        image: sword2,
        table: { columns: ['garums', 'asmens', 'rokturis'], rows: [['līdz 120 cm', 'līdz 85 cm', 'līdz 25 cm']] },
        heading: 'Bastarda zobens',
        // caption: 'Taisīt savu bastarda zobenu',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            Veikalā pirkts speciāls LARP bastarda zobens, vai paštaisīts:
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>Lietojams vienā vai divās rokās, pēc izvēles</ListItem>
                <ListItem>Ar ne-šķīstošu serdeni (bambuss nederēs)</ListItem>
                <ListItem>
                    Mīkstināts no visām asmeņa un roku sarga (guard) pusēm un arī gala.
                    Mīkstumu pārbauda iesitot īpašniekam un paskatoties viņa reakciju.
                </ListItem>
                <ListItem>Nav "bezsvara" smagumā</ListItem>
            </List>
        </Typography>),
    },
    {
        image: sword3,
        table: { columns: ['garums', 'asmens', 'rokturis'], rows: [['līdz 140 cm', 'līdz 115 cm', '25+ cm']] },
        heading: 'Divroku zobens',
        // caption: 'Taisīt savu divroku zobenu',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            Veikalā pirkts speciāls LARP divroku zobens, vai paštaisīts:
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>Lietojams divās rokās</ListItem>
                <ListItem>Ar ne-šķīstošu serdeni (bambuss nederēs)</ListItem>
                <ListItem>
                    Mīkstināts no visām asmeņa un roku sarga (guard) pusēm un arī gala.
                    Mīkstumu pārbauda iesitot īpašniekam un paskatoties viņa reakciju.
                </ListItem>
                <ListItem>Nav "bezsvara" smagumā</ListItem>
            </List>
        </Typography>),
    },
    {
        image: bow,
        heading: 'Loks/arbalets',
        // caption: 'Taisīt savas bultas',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>
                    Netīšām nebojā citus ieročus, kur tam var trāpīt (asas metāla dekorācijas, utml.)
                </ListItem>
                <ListItem>
                    Atvilkšanas spēks (draw weight): vēlams 10kg (22 lbs), ne vairāk kā 14 kg (30 lbs)
                </ListItem>
                <ListItem>Vienam lokam līdz 4 bultām</ListItem>
            </List>
            <Typography variant='h3'>Bultas</Typography>
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>
                    Galam jābūt mīkstinātam tā, lai korpuss nekādā gadījumā
                    nevar mīkstinājumam izdurties cauri, vai nolocīt to sānis.
                </ListItem>
                <ListItem>
                    Mīkstinātais uzgalis ir vismaz 5 cm diametrā (lai nevar nodarīt bojājumus acīm).
                </ListItem>
            </List>
        </Typography>),
    },
    {
        image: shield,
        heading: 'Vairogs',
        // caption: 'Taisīt savu vairogu',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>
                    Netīšām nebojā citus ieročus, kur tam var trāpīt (plika koka mala, asas metāla dekorācijas, utml.)
                </ListItem>
                <ListItem>Ieteicama ādas apdare</ListItem>
                <ListItem>Līdz 85cm diametrā vai 70x90 cm četrstūris/trijstūris</ListItem>
            </List>
        </Typography>),
    },
    {
        image: buckler,
        heading: 'Rokas vairogs (buckler)',
        // caption: 'Taisīt savu buckler',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>
                    Netīšām nebojā citus ieročus, kur tam var trāpīt (plika koka mala, asas metāla dekorācijas, utml.)
                </ListItem>
                <ListItem>Ieteicama ādas apdare</ListItem>
                <ListItem>Līdz 35cm diametrā</ListItem>
            </List>
        </Typography>),
    },
    {
        image: pouch,
        heading: 'Somiņa (pouch)',
        // caption: 'Taisīt savu somiņu',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>Var piestiprināt pie jostas vai valkāt pār plecu.</ListItem>
                <ListItem>Vismaz dūres tilpuma izmērā (lai ielien burkas vāciņš).</ListItem>
                <ListItem>Netīšām nebojā ieročus, (asas metāla dekorācijas, utml.)</ListItem>
            </List>
        </Typography>),
    },
    {
        image: armor,
        heading: 'Bruņas',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>
                    Vizuāli izskatās pēc bruņām,
                    <strong>nevar sajaukt ar drēbēm.</strong>
                </ListItem>
                <ListItem>Netīšām nebojā citus ieročus, kur tam var trāpīt (asas metāla dekorācijas, utml.)</ListItem>
                <ListItem>Materiāls: āda, neīsto bruņu plastmasa (EVA/craft foam), metāls</ListItem>
            </List>
        </Typography>),
    },
    {
        image: ram,
        heading: 'Aplenkuma ekipējums (battering ram)',
        content: (<Typography component="div" sx={{ marginTop: 2, marginBottom: 2 }}>
            Var uztaisīt spēles poligonā uz vietas. Nepieciešams:
            <List sx={{ listStyle: 'disc', marginLeft: 3, li: { padding: 0, display: 'list-item' } }}>
                <ListItem>2 striķi un nazis</ListItem>
                <ListItem>Mežā atrasts vai no malkas paņemts bluķis, bez zariem</ListItem>
                <ListItem>Bluķis vismaz 1 m garš un 15cm diametrā</ListItem>
            </List>
            Apsien striķus ap bluķi un gatavs!
        </Typography>),
    },
]
