import React from 'react'

import { Box, Grid, Typography } from '@material-ui/core'

import emoji_swords from 'assets/equipment/emoji_swords_small.png'

import { createFC, useBreakpoint } from 'src/common'
import { ClothesCard } from 'src/components/ClothesCard'
import { EquipmentCard } from 'src/components/EquipmentCard'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'

import { CLOTHES } from '../Clothes/common'

import { EQUIPMENT } from './common'


export default createFC(import.meta.url)(({ theme }) => {
    const isLgUp = useBreakpoint('lg')
    const isSmUp = useBreakpoint('sm')

    return (
        <Container
            overline="Ko ņemt kaujas laukam"
            title="Ekipējums"
            description="Uz vietas ieroci saņemsi, bet veikalā pērkamie, protams, labāki."
        >
            <Box sx={{
                background: 'linear-gradient(154.17deg, #E0E0E0 0%, #FAFAFA 83.69%);',
                padding: 4,
                margin: isSmUp ? 'initial' : -2,
                marginBottom: isSmUp ? 'initial' : 0,
            }}
            >
                <Grid container spacing={4}>
                    <Grid item xs={12} lg={7}>
                        <Box sx={{
                            display: 'flex',
                            height: '100%',
                            alignItems: 'center',
                            flexDirection: isLgUp ? 'row' : 'column',
                        }}
                        >
                            <Box component="img" height={80} width={80} src={emoji_swords} sx={{ marginRight: 5 }} />
                            <Typography>
                                Atšķirībā no citiem LARP, Tronī <strong>katram dalībniekam ir ierocis</strong>, kas pārsvarā ir īsais zobens ⚔️.
                                Taču tas nav vienīgais ekipējuma veids, ko var ņemt līdzi vai aizņemties no organizatoriem.
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={12} lg={5}>
                        <Typography>
                            Visam ekipējumam spēlē ir jābūt LARP drošam. Tas nozīmē, ka visu spēles ekipējumu pirms
                            spēles pārbauda organizatori: ekipējumu ieliek cenu kategorijās un pārbauda, vai ekipējums
                            ir drošs un nenodarīs miesas vai ekipējuma bojājumus un to drīkstēs izmantot spēlē.
                        </Typography>
                    </Grid>
                </Grid>
            </Box>
            <Section heading="Tuvcīņas ieroči">
                <EquipmentCard item={EQUIPMENT[1]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
                <EquipmentCard item={EQUIPMENT[2]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
                <EquipmentCard item={EQUIPMENT[3]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
                <EquipmentCard item={EQUIPMENT[4]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
            </Section>
            <Section heading="Tālcīņas ieroči">
                <EquipmentCard item={EQUIPMENT[5]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
            </Section>
            <Section heading="Vairogi">
                <EquipmentCard item={EQUIPMENT[6]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
                <EquipmentCard item={EQUIPMENT[7]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
            </Section>
            <Section heading="Cits ekipējums">
                <EquipmentCard item={EQUIPMENT[8]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
                <EquipmentCard item={EQUIPMENT[9]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
                <EquipmentCard item={EQUIPMENT[10]} chUp={false} sx={{ borderTop: '1px solid #EEEEEE' }} />
            </Section>
            <Section heading="Pieejamais ekipējums">
                <ClothesCard
                    item={CLOTHES[1]}
                    sxBox={{ background: theme.palette.background.default }}
                />
            </Section>
        </Container>
    )
}) /* =============================================================================================================== */
