import React from 'react'

import { Box, Grid } from '@material-ui/core'
import { useNavigate } from 'react-router'

import bannerLg from 'assets/banner-throne10-lg.jpg'
import banner from 'assets/banner-throne10.jpg'
import emoji_register from 'assets/emoji_register.png'
import emoji_rules from 'assets/emoji_rules.png'
import emoji_swords from 'assets/equipment/emoji_swords_small.png'

import { createFC, useBreakpoint } from 'src/common'
import { ItemCard } from 'src/components/ItemCard'
import SectionFaq from 'src/components/SectionFaq'
import SectionThisYear from 'src/components/SectionThisYear'
import { UniversalLink } from 'src/components/UniversalLink'
import { ROUTE } from 'src/constants'
import Container from 'src/layouts/DefaultLayout/Container'
import Section from 'src/layouts/DefaultLayout/Section'


import Footer from './Footer'


interface IProps {
}


export default createFC(import.meta.url)<IProps>(({ children, theme }) => {
    const mdUp = useBreakpoint('md')
    const lgUp = useBreakpoint('lg')
    const navigate = useNavigate()

    return (
        <Container
            banner={(
                <Box
                    component='img'
                    src={lgUp ?bannerLg : banner}
                    sx={{ display: 'block', objectFit: 'cover', width: '100%', height: 420 }}
                />
            )}
            overline="Spēles 2024. gadā"
            title="Tronis 2024"
            description="Visa svarīgā informācija par šī gada spēlēm."
            footer={<Footer />}
        >
            <Box sx={{ position: 'relative', [theme.breakpoints.up('md')]: { marginBottom: theme.spacing(4) } }}>
                <Section
                    id='Kā sagatavoties?'
                    heading={mdUp ? undefined : 'Kā sagatavoties?'}
                    sx={{
                        [theme.breakpoints.up('md')]: { position: 'absolute', bottom: 0, zIndex: 1, margin: theme.spacing(0, -8) },
                        [theme.breakpoints.up('lg')]: { margin: 0 },
                    }}
                >
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6} md={4}>
                            <UniversalLink href={ROUTE.RULES} onClick={(e) => {e.preventDefault(); navigate(ROUTE.RULES)}}>
                                <ItemCard
                                    sx={{ width: '100%', borderColor: theme.palette.primary.border, backgroundColor: theme.palette.background.paper }}
                                    item={{ heading: 'Izlasi noteikumus', image: emoji_rules, paragraph: 'Zini ko sagaidīt no spēles, un ko tajā varēsi darīt. (Noteikumi angļu valodā)' }}
                                />
                            </UniversalLink>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4}>
                            <UniversalLink href={ROUTE.RULES} onClick={(e) => {e.preventDefault(); navigate(ROUTE.EVENTS)}}>
                                <ItemCard
                                    sx={{ width: '100%', borderColor: theme.palette.primary.border, backgroundColor: theme.palette.background.paper }}
                                    item={{ heading: 'Apmeklē pasākumus*', image: emoji_swords, paragraph: <>Nāc uz treniņiem un kļūsti par zobencīņu ekspertu!<br />* nav obligāti</> }}
                                />
                            </UniversalLink>
                        </Grid>
                        <Grid item xs={12} sm={12} md={4}>
                            <UniversalLink href={ROUTE.REGISTRATION} onClick={(e) => {e.preventDefault(); navigate(ROUTE.REGISTRATION)}}>
                                <ItemCard
                                    sx={{ width: '100%', borderColor: theme.palette.primary.border, backgroundColor: theme.palette.background.paper }}
                                    item={{ heading: 'Reģistrējies', image: emoji_register, paragraph: 'Laicīgi rezervē savu vietu spēlē un komandā, jo vietu skaits ir ierobežots!' }}
                                />
                            </UniversalLink>
                        </Grid>
                    </Grid>
                </Section>
            </Box>
            <SectionThisYear />
            {/* <SectionEvents /> */}
            <SectionFaq />
            <Grid>
                {/* <Section heading='Sākas treniņu sezona!' overline='jaunumi'>
                    <Typography paragraph>
                        Cupidatat ullamco in proident velit cupidatat fugiat quis ad mollit nulla enim. Non id aliquip magna sint. Consequat nisi nisi cillum minim ad proident pariatur enim fugiat ad. Nulla eiusmod enim deserunt ea. Ea quis incididunt velit consequat ullamco proident laborum. Culpa non ipsum dolore eu cillum.
                    </Typography>
                    <Typography paragraph>
                        Proident aliquip minim incididunt adipisicing occaecat sit ad incididunt enim. Amet dolore ex mollit ad. Consectetur esse aliqua velit tempor laboris consequat duis ea ex adipisicing. Minim fugiat ullamco eiusmod et eiusmod.
                    </Typography>
                </Section>
                <Section heading='Tuvākie notikumi' overline='notikumu kalendārs'>
                </Section> */}
            </Grid>
        </Container>
    )
}) /* =============================================================================================================== */
