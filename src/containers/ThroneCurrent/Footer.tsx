import React from 'react'

import { Box, Button, Grid, Typography, alpha } from '@material-ui/core'
import { useNavigate } from 'react-router'

import cta_background from 'assets/cta_background.png'

import { createFC } from 'src/common'


export default createFC(import.meta.url)(({ children, theme }) => {
    const navigate = useNavigate()

    return (
        <Box sx={{
            backgroundImage: `url(${cta_background})`,
            backgroundPositionY: 'bottom',
            backgroundSize: 'cover',
            padding: theme.spacing(4, 4),
            [theme.breakpoints.up('sm')]: {
                padding: theme.spacing(4, 12),
            },
        }}
        >
            <Box sx={{
                backgroundColor: alpha('#000', 0.6),
                color: theme.palette.primary.contrastText,
                padding: theme.spacing(3, 2),
                maxWidth: 360,
                [theme.breakpoints.down('sm')]: {
                    textAlign: 'center',
                },
            }}
            >
                <Typography color='inherit' align='center' variant='h2' gutterBottom>
                    Piesakies spēlei
                </Typography>
                <Typography color='inherit' align='center' paragraph>
                    Atrodi visu info par reģistrēšanos vienuviet. Nav komandas? Pievienojies Troņa Discord serverim un atrodi komandu tur!
                </Typography>
                <Grid container justifyContent='center'>
                    <Button variant='contained' color='secondary' href='/registration' onClick={(e) => {e.preventDefault(); navigate('/registration')}}>
                        Pieteikties
                    </Button>
                </Grid>
            </Box>
        </Box>
    )
}) /* =============================================================================================================== */
