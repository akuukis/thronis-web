import React from 'react'

import { BrowserRouter, Route, Routes } from 'react-router-dom'

import { ROUTE } from 'src/constants'

import App from './containers/App'
import Clothes from './containers/Clothes'
import Contacts from './containers/Contacts'
import Equipment from './containers/Equipment'
import EventCalendar from './containers/Events'
import FAQ from './containers/FAQ'
import History from './containers/History'
import Home from './containers/Home'
import Items from './containers/Items'
import Larp from './containers/Larp'
import Mini from './containers/Mini'
import Registration from './containers/Registration'
import Rules from './containers/Rules'
import ThroneCurrent from './containers/ThroneCurrent'
import Venue from './containers/Venue'


export default (): JSX.Element => (
    <BrowserRouter>
        <Routes>
            <Route path="*" element={<App />}>
                <Route path={ROUTE.CURRENT} element={<ThroneCurrent />} />
                <Route path={ROUTE.HISTORY} element={<History />} />
                <Route path={ROUTE.LARP} element={<Larp />} />
                <Route path={ROUTE.CONTACTS} element={<Contacts />} />
                <Route path={ROUTE.VENUE} element={<Venue />} />
                <Route path={ROUTE.MINI} element={<Mini />} />
                <Route path={ROUTE.ITEMS} element={<Items />} />
                <Route path={ROUTE.EQUIPMENT} element={<Equipment />} />
                <Route path={ROUTE.CLOTHES} element={<Clothes />} />
                <Route path={ROUTE.REGISTRATION} element={<Registration />} />
                <Route path={ROUTE.RULES} element={<Rules />} />
                <Route path={ROUTE.FAQ} element={<FAQ />} />

                <Route path={ROUTE.EVENTS} element={<EventCalendar />} />
                <Route path="/" element={<Home />} />
            </Route>
        </Routes>
    </BrowserRouter>
)
