/* eslint-disable import/no-unassigned-import */
/* eslint-disable import/no-unused-modules */
import 'core-js/stable'
import 'regenerator-runtime/runtime'

import React from 'react'

import { CssBaseline } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/core'
import { configure } from 'mobx'
import { render } from 'react-dom'

import { MY_LIGHT_THEME } from './common'
import routes from './routes'


// enable MobX strict mode
configure({ enforceActions: 'always' })

render((
    <ThemeProvider theme={MY_LIGHT_THEME}>
        <CssBaseline />
        {routes()}
    </ThemeProvider>
),
document.getElementById('root'),
)
