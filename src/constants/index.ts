export enum ROUTE {
    HOME = '/',
    CURRENT = '/next',
    HISTORY = '/history',
    LARP = '/larp',
    CONTACTS = '/contacts',
    VENUE = '/venue',
    MINI = '/mini',
    ITEMS = '/items',
    EQUIPMENT = '/equipment',
    CLOTHES = '/clothes',
    REGISTRATION = '/registration',
    RULES = '/rules',
    FAQ = '/FAQ',

    EVENTS = '/events',
    TEST = '/test',
}
